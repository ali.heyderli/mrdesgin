<?php

namespace backend\controllers;

use common\components\AdminController;
use common\models\FileUtils;
use Yii;
use common\models\Partners;
use backend\models\PartnersSerach;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PartnersController implements the CRUD actions for Partners model.
 */
class PartnersController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnersSerach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partners model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Partners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partners();

        if ($model->load(Yii::$app->request->post())) {
            $mainImg = '';
            if (array_key_exists('Partners', $_FILES)) {
                if (!empty($_FILES['Partners']['name']['image'])) {
                    $ext = FileUtils::getType($_FILES['Partners']['type']['image']);
                    $imageName = substr(sha1(time() . rand(1000, 9999)), 0, 12) . '' . $ext;
                    $mainImg = Yii::getAlias('@partnersRoot/') . $imageName;
                    $model->image = $imageName;
                }
            }
            if ($model->save()) {
                if (array_key_exists('Partners', $_FILES)) {
                    if (!empty($_FILES['Partners']['name']['image'])) {
                        $prImage = $_FILES['Partners']['tmp_name']['image'];
                        $imgInfo = getimagesize($prImage);
                        Image::thumbnail($prImage, $imgInfo[0], $imgInfo[1])->save($mainImg, ['quality' => 80]);
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Partners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            if (array_key_exists('Partners', $_FILES)) {
                if (!empty($_FILES['Partners']['name']['image'])) {
                    if (!$model->isNewRecord && $model->image) {
                        $imgUrl = Yii::getAlias('@partnersRoot/').$model->image;
                        if (file_exists($imgUrl)) {
                            unlink($imgUrl);
                        }
                        if (file_exists($imgThumbUrl)) {
                            unlink($imgThumbUrl);
                        }
                    }

                    $ext = FileUtils::getType($_FILES['Partners']['type']['image']);
                    $imageName = substr(sha1(time() . rand(1000, 9999)), 0, 12) . $ext;
                    $mainImg = Yii::getAlias('@partnersRoot/') . $imageName;

                    $model->image = $imageName;
                }
            }
            if ($model->save()) {
                if (array_key_exists('Partners', $_FILES)) {
                    if (!empty($_FILES['Partners']['name']['image'])) {
                        $prImage = $_FILES['Partners']['tmp_name']['image'];
                        $imgInfo = getimagesize($prImage);
                        Image::thumbnail($prImage, $imgInfo[0], $imgInfo[1])->save($mainImg, ['quality' => 80]);
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Partners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Partners::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }
}
