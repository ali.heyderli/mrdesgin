<?php

namespace backend\controllers;

use common\components\AdminController;
use common\models\SourceMessage;
use Yii;
use common\models\Message;
use backend\models\MessageSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MessageController implements the CRUD actions for Message model.
 */
class MessageController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Message models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Message model.
     * @param integer $id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $language)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $language),
        ]);
    }

    /**
     * Creates a new Message model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Message();

        if (Yii::$app->request->isPost) {

            If (array_key_exists('Message', $_POST)) {
                $model2 = new SourceMessage();
                $model2->category = 'main';
                $model2->message = $_POST['Message']['translation'];
                if ($model2->save()) {
                    foreach ($_POST['Message']['translation2'] as $lang => $value) {
                        $model1 = new Message();
                        $model1->id = $model2->id;
                        $model1->language = $lang;
                        $model1->translation = $value;
                        $model1->save();
                    }

                    return $this->redirect(['view', 'id' => $model1->id, 'language' => $model1->language]);
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Message model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $models = Message::find()->where(['message.id' => $id])->joinWith('source')->indexBy('language')->asArray()->all();

        if (Yii::$app->request->isPost) {
            If (array_key_exists('Message', $_POST)) {
                $model2 = SourceMessage::findOne($id);
                $model2->message = $_POST['Message']['translation'];
                if ($model2->save()) {
                    foreach ($_POST['Message']['translation2'] as $lang => $value) {
                        $model1 =  Message::findOne(['id' => $id, 'language' => $lang]);
                        if($model1){
                            $model1->translation = $value;
                            $model1->save();
                        }
                        else {
                            $model3 = new Message();
                            $model3->id = $model2->id;
                            $model3->language = $lang;
                            $model3->translation = $value;
                            $model3->save(false);
                        }
                    }

                    return $this->redirect(['view', 'id' => $id, 'language' => Yii::$app->language]);
                }

            }


        } else {
            return $this->render('update', [
                'models' => $models,
            ]);
        }
    }

    /**
     * Deletes an existing Message model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $language)
    {
        $this->findModel($id, $language)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Message model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $language
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $language)
    {
        if (($model = Message::findOne(['id' => $id, 'language' => $language])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }
}
