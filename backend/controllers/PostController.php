<?php

namespace backend\controllers;

use common\components\AdminController;
use common\models\PostComplate;
use Yii;
use common\models\Post;
use common\models\PostSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $posts = Post::find()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'posts' => $posts
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'complete' => PostComplate::find()->where(['post_id' => $id])->one(),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionComplete($id)
    {
        $model = new PostComplate();
        $post = $this->findModel($id);
        $model->setScenario('insert');
        if ($model->load(Yii::$app->request->post()) ) {
            $model->post_id = $id;
            if($model->save()) {
                $post->completed = 1;
                $post->save(false);
                $params = [
                    'model' => $post,
                ];
                Yii::$app->mailer->compose(['html' => 'post-complete'],$params)
                    ->setTo($post->user->email)
                    ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                    ->setSubject('Hazır Post')
                    ->send();
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('complete', [
            'model' => $model,
        ]);
    }


    public function actionEditComplete($id)
    {
        $model = $this->findCompletedModel($id);
        $post = $this->findModel($model->post_id);
        $model->setScenario('insert');
        if ($model->load(Yii::$app->request->post()) ) {
            if($model->save()) {
                $post->completed = 1;
                $post->save(false);
                $params = [
                    'model' => $post,
                ];
                Yii::$app->mailer->compose(['html' => 'post-complete'],$params)
                    ->setTo($post->user->email)
                    ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                    ->setSubject('Hazır Post')
                    ->send();
                return $this->redirect(['view', 'id' => $model->post_id]);
            }
        }

        return $this->render('complete', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }


    protected function findCompletedModel($id)
    {
        if (($model = PostComplate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }
}
