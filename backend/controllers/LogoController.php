<?php

namespace backend\controllers;

use common\models\LogoComplete;
use common\models\PostComplate;
use Yii;
use common\models\Logo;
use common\models\LogoSearch;
use common\components\AdminController;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogoController implements the CRUD actions for Logo model.
 */
class LogoController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Logo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Logo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Logo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Logo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Logo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionComplete($id)
    {
        $model = new LogoComplete();
        $logo = $this->findModel($id);
        $model->setScenario('insert');
        if ($model->load(Yii::$app->request->post()) ) {
            $model->logo_id = $id;
            if($model->save()) {
                $logo->completed = 1;
                $logo->save(false);
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('complete', [
            'model' => $model,
        ]);
    }

    public function actionEditComplete($id)
    {
        $model = $this->findCompletes($id);
        $model->setScenario('insert');
        $logo = $this->findModel($model->logo_id);

        if ($model->load(Yii::$app->request->post()) ) {
            if($model->save()) {
                $logo->completed = 1;
                $logo->save(false);
                $params = [
                    'model' => $logo,
                ];
                Yii::$app->mailer->compose(['html' => 'logo-complete'],$params)
                    ->setTo($logo->user->email)
                    ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                    ->setSubject('Hazır logo')
                    ->send();
                return $this->redirect(['view', 'id' => $model->logo_id]);
            }
        }
        return $this->render('complete', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing Logo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Logo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }


    protected function findCompletes($id)
    {
        if (($model = LogoComplete::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }
}
