<?php

namespace backend\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admintheme/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'admintheme/bower_components/font-awesome/css/font-awesome.min.css',
        'admintheme/bower_components/simple-line-icons/css/simple-line-icons.css',
        'admintheme/bower_components/weather-icons/css/weather-icons.min.css',
        'admintheme/bower_components/themify-icons/css/themify-icons.css',
        'admintheme/dist/css/main.css',

    ];
    public $js = [
        'admintheme/assets/js/modernizr-custom.js',
        'admintheme/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js',
        'admintheme/bower_components/autosize/dist/autosize.min.js',
        'admintheme/dist/js/main.js',
    ];
    public $depends = ['yii\web\JqueryAsset','yii\bootstrap\BootstrapAsset'];

}
