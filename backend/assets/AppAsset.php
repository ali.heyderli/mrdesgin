<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admintheme/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'admintheme/bower_components/font-awesome/css/font-awesome.min.css',
        'admintheme/bower_components/simple-line-icons/css/simple-line-icons.css',
        'admintheme/bower_components/weather-icons/css/weather-icons.min.css',
        'admintheme/bower_components/weather-icons/css/weather-icons.min.css',
        'admintheme/dist/css/main.css',
        'admintheme/bower_components/rickshaw/rickshaw.min.css',
        'admintheme/assets/js/jquery-easy-pie-chart/easypiechart.css',
        'admintheme/assets/js/horizontal-timeline/css/style.css',
        'admintheme/assets/js/modernizr-custom.js',
    ];
    public $js = [
        'admintheme/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'admintheme/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js',
        'admintheme/bower_components/autosize/dist/autosize.min.js',
        'admintheme/bower_components/highcharts/highcharts.js',
        'admintheme/bower_components/highcharts/highcharts-more.js',
        'admintheme/bower_components/highcharts/modules/exporting.js',
        'admintheme/bower_components/bower-jquery-sparkline/dist/jquery.sparkline.retina.js',
        'admintheme/assets/js/init-sparkline.js',
        'admintheme/assets/js/echarts/echarts-all-3.js',
        'admintheme/assets/js/jquery-easy-pie-chart/jquery.easypiechart.js',
        'admintheme/assets/js/horizontal-timeline/js/jquery.mobile.custom.min.js',
        'admintheme/assets/js/horizontal-timeline/js/main.js',
        'admintheme/dist/js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
