<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PostPlan;

/**
 * PostPlanSearch represents the model behind the search form of `common\models\PostPlan`.
 */
class PostPlanSearch extends PostPlan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'simple_post', 'creative_post', 'photo_shot', 'video_shot', 'animation', 'created_at', 'updated_at'], 'integer'],
            [['plan_name', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostPlan::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'simple_post' => $this->simple_post,
            'creative_post' => $this->creative_post,
            'photo_shot' => $this->photo_shot,
            'video_shot' => $this->video_shot,
            'animation' => $this->animation,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'plan_name', $this->plan_name]);

        return $dataProvider;
    }
}
