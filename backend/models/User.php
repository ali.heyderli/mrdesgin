<?php

namespace backend\models;

use yii\helpers\VarDumper;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $plan_id
 * @property int $balance
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    public $password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [[ 'plan_id', 'balance',], 'required'],
            [['plan_id', 'balance', 'status', 'created_at', 'updated_at'], 'integer'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'username' => Yii::t('main', 'Username'),
            'auth_key' => Yii::t('main', 'Auth Key'),
            'password_hash' => Yii::t('main', 'Password Hash'),
            'password_reset_token' => Yii::t('main', 'Password Reset Token'),
            'email' => Yii::t('main', 'Email'),
            'plan_id' => Yii::t('main', 'Plan ID'),
            'balance' => Yii::t('main', 'Balance'),
            'status' => Yii::t('main', 'Status'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    //regiter user at admin panel

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function register()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->sms_plan_id = $this->sms_plan_id;
        $user->balance = $this->balance;
        $user->password = $this->password;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->validate();

        if ($user->save()) {
            $auth = \Yii::$app->authManager;
            $authorRole = $auth->getRole('user');
            //assign new role
            $auth->assign($authorRole, $user->id);
            return  $user;

        }
        else {
            return null;
        }

    }
}
