<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 11/17/18
 * Time: 22:26
 */
namespace backend\models;

use tecnocen\yearcalendar\data\DataItem;
use tecnocen\yearcalendar\data\JsExpressionHelper;
use yii\db\ActiveRecord;

class TestCalendar extends ActiveRecord implements DataItem
{
    public function getName()
    {
        return $this->name;
    }

    public function getStartDate()
    {
        return JsExpressionHelper::parse($this->start_date);
    }

    public function getEndDate()
    {
        return JsExpressionHelper::parse($this->end_date);
    }

}