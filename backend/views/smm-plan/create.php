<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SmmPlan */

$this->title = Yii::t('main', 'Create Smm Plan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Smm Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
            </div>
        </div>
    </div>
</div>
