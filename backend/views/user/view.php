<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">
    <p>
        <?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            [
                'attribute' => 'status',
                'value' => function($data)
                {
                    return ($data->status) ? 'Aktiv' : 'Deaktiv';
                }
            ],
            [
                    'attribute' => 'created_at',
                    'value' => function($data)
                    {
                        return date('Y-d-m',$data->created_at);
                    }
            ],
        ],
    ]) ?>
                    <h3>Aktiv planlar</h3>
        <?php foreach ($model->subscribes as $s):
            $name =  (isset($s->plan->plan_name)) ? $s->plan->plan_name : $s->plan->name;
            echo '<span class="badge badge-secondary">'.$name.'</span><br>';

        endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>