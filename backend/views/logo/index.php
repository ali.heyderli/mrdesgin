<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LogoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Logos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Create Logo'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'logo_name',
            [
                    'attribute' => 'user_id',
                    'value' => function($data)
                    {
                       return $data->user->username;
                    }
            ],
            [
                'attribute' => 'completed',
                'value' => function($data) {
        switch ($data->completed) {
            case 1:
                $status = 'Hazırdır';
                break;
            case 2:
                $status = 'Bəyənilmədi';
                break;
            default:
                $status =  "Hazır deyil";
                break;

        }

                    return $status;
                }
            ],
            //'created_at',
            //'updated_at',
            //'company_name',
            //'logo_name',
            //'slogan',
            //'about_company:ntext',
            //'colors',
            //'other_logos',
            //'company_desc',
            //'logo_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
            </div>
        </div>
    </div>
</div>
