<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Logos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">
    <p>
        <?php if (!$model->complete): ?>
        <?= Html::a(Yii::t('main', 'Complete'), ['complete', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php else: ?>
            <?= Html::a(Yii::t('main', 'Edit Completed'), ['edit-complete', 'id' => $model->complete->id], ['class' => 'btn btn-primary']) ?>

        <?php endif;?>
        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($data) {
                    return  $data->user_id ? $data->user->username : '';
                }
            ],
            'completed',
            [
                'attribute' => 'created_at',
                'value' => function($data) {
                    return  $data->created_at ? date('Y-m-d', $data->created_at) : '';
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data) {
                    return  $data->updated_at ? date('Y-m-d', $data->updated_at) : '';
                }
            ],
            'company_name',
            'logo_name',
            'slogan',
            'about_company:ntext',
            'colors',
            'other_logos',
            'company_desc',
            [
                'attribute' => 'logo_type',
                'value' => function($data) {
                    return  \common\models\Logo::getTypes()[$data->logo_type];
                }
            ],
            [
                    'attribute' => 'type_1',
                    'value' => function($data) {
                        return  \common\models\Logo::getForms()[$data->type_1];
                }
            ],
            [
                    'attribute' => 'type_2',
                    'value' => function($data) {
                        return  \common\models\Logo::getForms()[$data->type_2];
                }
            ],
//            [
//                'attribute' => 'type_3',
//                'value' => function($data) {
//                    return  \common\models\Logo::getForms()[$data->type_3];
//                }
//            ],
            [
                'attribute' => 'type_4',
                'value' => function($data) {
                    return  \common\models\Logo::getForms()[$data->type_4];
                }
            ]
        ],
    ]) ?>

</div>
            </div>
        </div>
    </div>
</div>
