<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 2019-01-30
 * Time: 15:58
 */

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">

                    <div class="portfolio-form">

                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                        <div class="row">
                            <div class="col-md-12">
                                <?=$form->field($model, 'file')->widget(FileInput::classname(), [
                                    'pluginOptions' => [
                                        'initialPreview' => $model->file ? Yii::getAlias('@post/').$model->file : false,
                                        'initialPreviewAsData' => !empty($model->file),
                                        'showCaption' => false,
                                        'showRemove' => false,
                                        'showUpload' => false,
                                        'browseLabel' =>  $model->isNewRecord ? 'Select logo image' : 'Change logo image'
                                    ],
                                    'options' => [
                                        'multiple' => false
                                    ],
                                ]);?>
                            </div>
                            <!--        <div class="col-md-6">-->
                            <!--            --><?//=$form->field($model, 'image_b')->widget(FileInput::classname(), [
                            //                'pluginOptions' => [
                            //                    'initialPreview' => $model->image_b ? Yii::getAlias('@portfolio/').$model->image_b : false,
                            //                    'initialPreviewAsData' => !empty($model->image_b),
                            //                    'showCaption' => false,
                            //                    'showRemove' => false,
                            //                    'showUpload' => false,
                            //                    'browseLabel' =>  $model->isNewRecord ? 'Select back image' : 'Change back image'
                            //                ],
                            //                'options' => [
                            //                    'accept' => 'image/*',
                            //                    'multiple' => false
                            //                ],
                            //            ]);?>
                            <!--        </div>-->
                        </div>
                        <?= $form->field($model, 'text')->textarea() ?>


                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('main', 'Upload'), ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
