<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'pcategory_id')->dropDownList($categories, ['prompt'=>'Choose portfolio category']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-md-12">
            <?=$form->field($model, 'image_f')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'initialPreview' => $model->image_f ? Yii::getAlias('@portfolio/').$model->image_f : false,
                    'initialPreviewAsData' => !empty($model->image_f),
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseLabel' =>  $model->isNewRecord ? 'Select front image' : 'Change front image'
                ],
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => false
                ],
            ]);?>
        </div>
<!--        <div class="col-md-6">-->
<!--            --><?//=$form->field($model, 'image_b')->widget(FileInput::classname(), [
//                'pluginOptions' => [
//                    'initialPreview' => $model->image_b ? Yii::getAlias('@portfolio/').$model->image_b : false,
//                    'initialPreviewAsData' => !empty($model->image_b),
//                    'showCaption' => false,
//                    'showRemove' => false,
//                    'showUpload' => false,
//                    'browseLabel' =>  $model->isNewRecord ? 'Select back image' : 'Change back image'
//                ],
//                'options' => [
//                    'accept' => 'image/*',
//                    'multiple' => false
//                ],
//            ]);?>
<!--        </div>-->
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
