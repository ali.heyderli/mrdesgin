<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */

$this->title = Yii::t('main', 'Create Portfolio');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Portfolios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>

</div>
</div>
</div>
</div>
</div>
