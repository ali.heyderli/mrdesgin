<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PricePlan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-plan-form">

    <ul class="nav nav-tabs">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <li class="<?= Yii::$app->language == $lang ? 'active':''?>">
                <a class="text-uppercase" data-toggle="tab" href="#section_<?=$lang?>"><?=$lang?></a>
            </li>
        <?php endforeach;?>
    </ul>
    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <div id="section_<?=$lang?>" class="tab-pane fade<?= Yii::$app->language == $lang ? ' in active':''?>">
                <?= $form->field($model, 'name'.(Yii::$app->language == $lang?'':'_'.$lang))->textInput(['maxlength' => 50]) ?>
                <?= $form->field($model, 'text'.(Yii::$app->language == $lang?'':'_'.$lang))->textarea(['rows' => 3]) ?>
            </div>
        <?php endforeach;?>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'price')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'count')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList([1 => 'Post', 2 => 'Logo'], ['prompt' => 'Select type']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'is_active')->checkbox() ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
