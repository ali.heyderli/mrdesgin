<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricePlan */

$this->title = Yii::t('main', 'Update Price Plan: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Price Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>