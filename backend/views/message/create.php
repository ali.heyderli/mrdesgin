<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Message */

$this->title = Yii::t('main', 'Create Message');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model,
        'isNewRecord' => true
    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>