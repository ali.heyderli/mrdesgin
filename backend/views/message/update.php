<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $models array */

$this->title = 'Update Message: ' . $models['en']['translation'];
$this->params['breadcrumbs'][] = ['label' => 'Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $models['en']['translation'], 'url' => ['view', 'id' => $models['en']['id'], 'language' => $models['en']['language']]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">
    <?= $this->render('_form', [
        'models' => $models,
        'isNewRecord' => false
    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>