<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-form">

    <ul class="nav nav-tabs">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <li class="<?= Yii::$app->language == $lang ? 'active':''?>"><a class="text-uppercase" data-toggle="tab" href="#section_<?=$lang?>"><?=$lang?></a></li>
        <?php endforeach;?>
    </ul>
    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <div id="section_<?=$lang?>" class="tab-pane fade<?= Yii::$app->language == $lang ? ' in active':''?>">
                <br/>
                <div class="form-group field-message-translation">
                    <label class="control-label" for="message-translation">Translation</label>
                    <textarea id="message-translation" class="form-control" name="Message[translation2][<?=$lang?>]" rows="6" aria-invalid="false"><?= isset($models[$lang]) ? $models[$lang]['translation'] : ''?></textarea>
                    <div class="help-block"></div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <?php  ?>
    <div class="form-group field-message-translation">
        <label class="control-label" for="message-translation">Key</label>
        <input type="text" id="message-translation" class="form-control"  value="<?= !$isNewRecord ? $models['az']['source']['message'] : ''?>" name="Message[translation]" >

        <div class="help-block"></div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($isNewRecord ? 'Create' : 'Update', ['class' => $isNewRecord ? 'btn btn-info btn-fill' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

