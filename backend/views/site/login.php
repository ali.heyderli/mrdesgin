<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="sign-in-wrapper">
    <div class="sign-container">
        <div class="text-center">
            <h2 class="logo"><?=Yii::$app->name?><!--<img src="/admin/admintheme/imgs/logo-dark.png" width="130px" alt=""/>--></h2>
            <h4>Login to Admin</h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'sign-in-form']); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox(['class' => 'i-checks']) ?>
        <div class="form-group">
            <?= Html::submitButton('Login', ['class' => 'btn btn-info btn-block', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="text-center copyright-txt">
            <small><?=Yii::$app->name?> - Copyright © <?=date('Y')?></small>
        </div>
    </div>
</div>
