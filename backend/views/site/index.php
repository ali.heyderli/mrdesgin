<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Mrdesign Admin';
?>


        <!--page title and breadcrumb start -->
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-title"> Mrdesign Dashboard
                </h1>
            </div>
            <div class="col-md-4">
                <ul class="breadcrumb pull-right">
                    <li>Home</li>
                    <li><a href="#" class="active">Dashboard</a></li>
                </ul>
            </div>
        </div>
        <!--page title and breadcrumb end -->

        <!--states start-->
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="panel short-states bg-danger">
                    <div class="pull-right state-icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="panel-body">
                        <h1 class="light-txt"><?=count($posts)?></h1>
                        <div class=" pull-right">53% <i class="fa fa-bolt"></i></div>
                        <strong class="text-uppercase">Post</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel short-states bg-info">
                    <div class="pull-right state-icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="panel-body">
                        <h1 class="light-txt"><?=$users?></h1>
                        <div class=" pull-right">65% <i class="fa fa-level-up"></i></div>
                        <strong class="text-uppercase">Users</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel short-states bg-warning">
                    <div class="pull-right state-icon">
                        <i class="fa fa-laptop"></i>
                    </div>
                    <div class="panel-body">
                        <h1 class="light-txt">$22,329</h1>
                        <div class=" pull-right">77% <i class="fa fa-level-down"></i></div>
                        <strong class="text-uppercase">Online Revenue</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel short-states bg-primary">
                    <div class="pull-right state-icon">
                        <i class="fa fa-pie-chart"></i>
                    </div>
                    <div class="panel-body">
                        <h1 class="light-txt">$268,188</h1>
                        <div class=" pull-right">88% <i class="fa fa-level-up"></i></div>
                        <strong class="text-uppercase">Total Profit</strong>
                    </div>
                </div>
            </div>
        </div>
        <!--states end-->


        <div class="row">

            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading">
                        Posts
                        <span class="tools pull-right">
                                            <a class="close-box fa fa-times" href="javascript:;"></a>
                                        </span>
                    </header>
                    <div class="panel-body">
                        <div id="b-line" style="height: px">
                            <?php
                            $events = array();
                            $Event = new \yii2fullcalendar\models\Event();
                            foreach ($posts as $post) {
                                $Event = new \yii2fullcalendar\models\Event();
                                $Event->id = $post->id;
                                $Event->title = $post->title;
                                $Event->url = Url::to(['/post/view', 'id' => $post->id ]);
                                $Event->start = date('Y-m-d\TH:i:s\Z',$post->due_date);
                                $events[] = $Event;
                            }

                            ?>

                            <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                                'events'=> $events,
                                'options' => [
                                    'lang' => 'en',
                                ],
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <header class="panel-heading">
                        Loqo
                        <span class="tools pull-right">
                                            <a class="close-box fa fa-times" href="javascript:;"></a>
                                        </span>
                    </header>
                    <div class="panel-body">
                        <div id="b-line" style="height: px">

                        </div>
                    </div>
                </div>
            </div>
        </div>



    <!--right side widget start-->
    <div class="ui-aside-right " ui-aside-right>

        <!--customer start-->
        <div class="aside-widget">
            <h5>Online Customer</h5>
            <ul class="contact-list">
                <li class="online">
                    <div class="media">
                        <a href="#" class="pull-left media-thumb">
                            <img alt="" src="imgs/a0.jpg" class="media-object">
                            <span class="online"></span>
                        </a>
                        <div class="media-body">
                            <strong>John Doe</strong>
                            <small>General Manager at TB</small>
                        </div>
                    </div><!-- media -->
                </li>
                <li class="online">
                    <div class="media">
                        <a href="#" class="pull-left media-thumb">
                            <img alt="" src="imgs/a1.jpg" class="media-object">
                            <span class="offline"></span>
                        </a>
                        <div class="media-body">
                            <strong>Jonathan Smith</strong>
                            <small>Lead Designer</small>
                        </div>
                    </div><!-- media -->
                </li>

                <li class="online">
                    <div class="media">
                        <a href="#" class="pull-left media-thumb">
                            <img alt="" src="imgs/a2.jpg" class="media-object">
                            <span class="busy"></span>
                        </a>
                        <div class="media-body">
                            <strong>Margarita Rose</strong>
                            <small>Human Resource Manager</small>
                        </div>
                    </div><!-- media -->
                </li>
                <li class="online">
                    <div class="media">
                        <a href="#" class="pull-left media-thumb">
                            <img alt="" src="imgs/a0.jpg" class="media-object">
                            <span class="away"></span>
                        </a>
                        <div class="media-body">
                            <strong>Mr. Kameron De</strong>
                            <small>Marketing Officer</small>
                        </div>
                    </div><!-- media -->
                </li>
                <li class="online">
                    <div class="media">
                        <a href="#" class="pull-left media-thumb">
                            <img alt="" src="imgs/a2.jpg" class="media-object">
                            <span class="offline"></span>
                        </a>
                        <div class="media-body">
                            <strong>Mr. Mosa</strong>
                            <small>Development Manager</small>
                        </div>
                    </div><!-- media -->
                </li>
            </ul>
        </div>
        <!--customer end-->

        <!--stock start-->
        <div class="aside-widget">
            <h5>Recent Stocks</h5>
            <table class="table">
                <tbody>
                <tr>
                    <td>DOWJ</td>
                    <td>19,764.00</td>
                    <td><small class="label label-success">+ 0.08%</small></td>
                </tr>
                <tr>
                    <td>AAPL</td>
                    <td>116.90</td>
                    <td><small class="label label-danger">- 0.29%</small></td>
                </tr>
                <tr>
                    <td>SBUX</td>
                    <td>50.33</td>
                    <td><small class="label label-warning">+ 0.23%</small></td>
                </tr>
                <tr>
                    <td>NKE</td>
                    <td>164.00</td>
                    <td><small class="label label-success">+ 0.08%</small></td>
                </tr>
                <tr>
                    <td>YHOO</td>
                    <td>764.00</td>
                    <td><small class="label label-danger">- 0.91%</small></td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--stock end-->

        <!--task start-->
        <div class="aside-widget">
            <h5>Task Pending</h5>
            <ul class="list-unstyled">
                <li>
                    <a href="#">
                        <div class="task-info">
                            Wp Development (66%)
                        </div>
                        <div class="progress progress-striped">
                            <div style="width: 66%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="66" role="progressbar" class="progress-bar progress-bar-warning">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="task-info">
                            Dashboard Design (80%)
                        </div>
                        <div class="progress progress-striped">
                            <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-success ">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="task-info">
                            Marketing (40%)
                        </div>
                        <div class="progress progress-striped">
                            <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-info">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="task-info">
                            Mobile App Development (33%)
                        </div>
                        <div class="progress progress-striped">
                            <div style="width: 33%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-danger">
                            </div>
                        </div>
                    </a>
                </li>
                <li class="text-center">
                    <a href="#" class="btn btn-sm btn-info btn-block">See All Pending Tasks</a>
                </li>
            </ul>
        </div>
        <!--task end-->

    </div>
    <!--right side widget end-->
