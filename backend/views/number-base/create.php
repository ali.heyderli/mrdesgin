<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NumberBase */

$this->title = Yii::t('main', 'Create Number Base');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Number Bases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model,
        'targets' => $targets
    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>
