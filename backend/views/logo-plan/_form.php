<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LogoPlan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logo-plan-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'plan_name')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'price')->textInput() ?>
        </div>
    </div>
    <?= $form->field($model, 'plan')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
