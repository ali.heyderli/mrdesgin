<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PostPlan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-plan-form">
    <ul class="nav nav-tabs">
        <?php foreach ($_SESSION['languages'] as $lang):?>
            <li class="<?= Yii::$app->language == $lang ? 'active':''?>">
                <a class="text-uppercase" data-toggle="tab" href="#section_<?=$lang?>"><?=$lang?></a>
            </li>
        <?php endforeach;?>
    </ul>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="tab-content">
            <?php foreach ($_SESSION['languages'] as $lang):?>
                <div id="section_<?=$lang?>" class="tab-pane fade<?= Yii::$app->language == $lang ? ' in active':''?>">
                    <?= $form->field($model, 'name'.(Yii::$app->language == $lang?'':'_'.$lang))->textInput(['maxlength' => 50]) ?>
                </div>
            <?php endforeach;?>
        </div>

        <div class="col-md-6">

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'simple_post')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'creative_post')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'photo_shot')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'video_shot')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'animation')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput() ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
