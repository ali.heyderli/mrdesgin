<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">

    <p>
        <?php if (!$model->complete): ?>
            <?= Html::a(Yii::t('main', 'Complete'), ['complete', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php else: ?>
            <?= Html::a(Yii::t('main', 'Edit Completed'), ['edit-complete', 'id' => $model->complete->id], ['class' => 'btn btn-primary']) ?>

        <?php endif;?>        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'title',
                            'description:ntext',
                            [
                                'attribute' => 'due_date',
                                'value' => function($data) {
                                    return date('Y-m-d H:i',$data->due_date);
                                }
                            ],
                            [
                                'attribute' => 'user_id',
                                'value' => function($data) {
                                    return $data->user->username;
                                }
                            ],
                            [
                                'attribute' => 'complated',
                                'value' => function($data) {
                                    switch ($data->completed) {
                                        case 1:
                                            $status = 'Hazırdır';
                                            break;
                                        case 2:
                                            $status = 'Bəyənilmədi';
                                            break;
                                        default:
                                            $status =  "Hazır deyil";
                                            break;

                                    }

                                    return $status;
                                }
                            ],

                            [
                                'attribute' => 'category_id',
                                'value' => function($data) {
                                    return \common\models\Post::getCategory($data->category_id);
                                }
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => function($data) {
                                    return date('Y-m-d H:i',$data->created_at);
                                }
                            ],
                        ],
                    ]) ?>
                    <div class="row">
                        <?php foreach ($model->images as $image):?>
                            <div class="col-md-4">
                                <a href="<?=Yii::getAlias('@post/').$image->image?>">
                                    <img class="img-responsive" src="<?=Yii::getAlias('@post/').$image->image?>">
                                </a>

                            </div>

                        <?php endforeach;?>
                    </div>
</div>
</div>
</div>
</div>
</div>
