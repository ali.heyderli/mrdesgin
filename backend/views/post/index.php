<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ui-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading panel-border"><?= Html::encode($this->title) ?></div>
                <div class="panel-body">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Create Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="col-md-12">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php
        $events = array();
        $Event = new \yii2fullcalendar\models\Event();
        foreach ($posts as $post) {
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $post->id;
            $Event->title = $post->title;
            $Event->url = Url::to(['/post/view', 'id' => $post->id ]);
            $Event->start = date('Y-m-d',$post->due_date);
            $Event->end = date('Y-m-d',$post->due_date);
            $events[] = $Event;
        }

        ?>

        <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
            'events'=> $events,
            'options' => [
                'lang' => 'en',
            ],
        ));
        ?>
    </div>

<!--    --><?//= GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//            'user_id',
//            'title',
//            'description:ntext',
//            'due_date',
//            //'completed',
//            //'created_at',
//            //'updated_at',
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>
</div>
</div>
