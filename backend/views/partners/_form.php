<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Partners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?=$form->field($model, 'image')->widget(FileInput::classname(), [
      'pluginOptions' => [
          'initialPreview' => $model->image ? Yii::getAlias('@partners/').$model->image : false,
          'initialPreviewAsData' => !empty($model->image),
          'showCaption' => false,
          'showRemove' => false,
          'showUpload' => false,
          'browseLabel' =>  $model->isNewRecord ? 'Select image' : 'Change media image'
      ],
        'options' => [
                'accept' => 'image/*',
                'multiple' => false
        ],
    ]);?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
