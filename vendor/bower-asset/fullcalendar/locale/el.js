import 'moment/locale/el';
import * as FullCalendar from 'fullcalendar';


/* Greek (el) initialisation for the jQuery UI date picker plugin. */
/* Written by Alex Cicovic (http://www.alexcicovic.com) */
FullCalendar.datepickerLocale('el', 'el', {
  closeText: "Bağla",
  prevText: "İrəli",
  nextText: "Geri",
  currentText: "Bugun",
  monthNames: [ "Yanvr","FEvral","Mart","Aprel","May","İyun",
    "İyul","Avqust","Sentyabr","Oktyabr","Noyabr","Decabr" ],
  monthNamesShort: [ "Yan", "Fev", "Mar", "Apr", "May", "İyn",
    "İyl", "Avq", "Sen", "Okt", "Noy", "Dek" ],
  dayNames: [ "Bazar", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"  ],
  dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
  dayNamesMin: [ "Su","Mo","Tu","We","Th","Fr","Sa" ],
  weekHeader: "Həftə",
  dateFormat: "dd/mm/yy",
  firstDay: 1,
  isRTL: false,
  showMonthAfterYear: false,
  yearSuffix: "" });


FullCalendar.locale("el", {
  buttonText: {
    month: "Ay",
    week: "həftə",
    day: "gün",
    list: "list"
  },
  allDayText: "Bütün gün",
  eventLimitText: function(n) {
    return "+ və " + n;
  noEventsMessage: "Göstəriləcək data yoxdur"
});
