<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 7/20/18
 * Time: 12:50 PM
 */
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //Delete all old data

        //Create Roles
        $admin = $auth->createRole('admin');
        $manager  = $auth->createRole('manager');
        $user = $auth->createRole('user');
        $subscribedUser = $auth->createRole('subscribedUser');

        //Add to DB
        $auth->add($admin);
        $auth->add($manager);
        $auth->add($user);
        $auth->add($subscribedUser);

        //Create permissions
        $viewAdminPage = $auth->createPermission('viewAdminPage');
        $viewAdminPage->description = 'View Admin page';

        $viewManagerPage = $auth->createPermission('viewManagerPage');
        $viewManagerPage->description = 'View manager page';

        $viewUserPage = $auth->createPermission('viewUserPage');
        $viewUserPage->description = 'View user page';

        $viewSubscribedPage = $auth->createPermission('viewSubscribedPage');
        $viewSubscribedPage->description = 'View subscribe pages';

        //Add to DB
        $auth->add($viewAdminPage);
        $auth->add($viewManagerPage);
        $auth->add($viewUserPage);
        $auth->add($viewSubscribedPage);


        $auth->addChild($manager, $viewManagerPage);
        $auth->addChild($user, $viewUserPage);
        $auth->addChild($subscribedUser, $viewSubscribedPage);
        $auth->addChild($admin, $manager);
        $auth->addChild($admin, $user);
        $auth->addChild($subscribedUser, $user);
        $auth->addChild($admin, $subscribedUser);
        $auth->addChild($admin, $viewAdminPage);

        $auth->assign($admin, 1);
        $auth->assign($user, 2);

    }
}