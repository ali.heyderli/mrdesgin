<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcategory`.
 */
class m181116_165635_create_pcategory_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pcategory', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'enabled' => $this->integer(1)->notNull()->defaultValue(1),
        ]);

        $this->createTable('pcategory_lang',[
            'id' => $this->primaryKey(),
            'pcategory_id' => $this->integer(11)->notNull(),
            'lang' => $this->string(2)->notNull(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createIndex('idx-pcategory_id', 'pcategory_lang','pcategory_id');
        $this->addForeignKey('fk-pcategory_lang-cat_id',
            'pcategory_lang',
            'pcategory_id',
            'pcategory',
            'id',
            'CASCADE',
            'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pcategory');
    }
}
