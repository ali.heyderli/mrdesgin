<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms_plan`.
 */
class m180721_094058_create_sms_plan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sms_plan', [
            'id' => $this->primaryKey(),
            'price' => $this->integer(10)->notNull(),
            'created_at' => $this->integer(10)->notNull(),
            'is_active' => $this->integer(1)->notNull()->defaultValue(0),
            'updated_at' => $this->integer(10)->notNull(),

        ]);
        $this->createTable('sms_plan_lang',[
            'id' => $this->primaryKey(),
            'sms_plan_id' => $this->integer(10),
            'name' => $this->string(100),
            'lang' => $this->string(5)
        ]);

        $this->createIndex('idx-sms_plan_id','sms_plan_lang','sms_plan_id');
        $this->addForeignKey('fk-sms_plan_id','sms_plan_lang','sms_plan_id','sms_plan','id','CASCADE','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sms_plan');
    }
}
