<?php

use yii\db\Migration;

/**
 * Class m181116_111340_cerate_partners_table
 */
class m181116_111340_cerate_partners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('partners', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'image' => $this->string(100)->notNull(),
                'created_at' => $this->integer(11)->notNull(),
                'updated_at' => $this->integer(11)->notNull()
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('partners');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181116_111340_cerate_partners_table cannot be reverted.\n";

        return false;
    }
    */
}
