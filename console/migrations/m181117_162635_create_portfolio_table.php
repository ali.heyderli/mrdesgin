<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portfolio`.
 */
class m181117_162635_create_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('portfolio', [
            'id' => $this->primaryKey(),
            'pcategory_id' => $this->integer(10)->notNull(),
            'name' => $this->string(255)->notNull(),
            'image_f' => $this->string(100)->notNull(),
            'image_b' => $this->string(100)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('idx-pcategory_id','portfolio','pcategory_id');
        $this->addForeignKey('fk-pcategory_id','portfolio','pcategory_id','pcategory','id',
            'CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('portfolio');
    }
}
