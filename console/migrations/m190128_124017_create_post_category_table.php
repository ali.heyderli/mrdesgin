<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_category`.
 */
class m190128_124017_create_post_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_category', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'enabled' => $this->integer(1)->notNull()->defaultValue(1),
        ]);

        $this->createTable('post_category_lang',[
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(11)->notNull(),
            'lang' => $this->string(2)->notNull(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createIndex('idx-category_id', 'post_category_lang','category_id');
        $this->addForeignKey('fk-category_lang-category_id',
            'post_category_lang',
            'category_id',
            'post_category',
            'id',
            'CASCADE',
            'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_category');
        $this->dropTable('post_category_lang');
    }
}
