<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscription`.
 */
class m181117_212615_create_subscription_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subscription', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(10)->notNull(),
            'price_plan_id' => $this->integer(10)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull()
        ]);
        $this->createIndex('idx-user_id','subscription','user_id');
        $this->addForeignKey('fk-user_id',
            'subscription',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE');
        $this->createIndex('idx-price_plan_id','subscription','price_plan_id');
        $this->addForeignKey('fk-price_plan_id',
            'subscription',
            'price_plan_id',
            'price_plan',
            'id',
            'CASCADE',
            'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('subscription');
    }
}
