<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m181117_233148_create_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(10)->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'due_date' => $this->integer(11)->notNull(),
            'completed' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);
        $this->createIndex('idx-user_id','post','user_id');
        $this->addForeignKey('fk-post-user_id',
            'post',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('posts');
    }
}
