<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_images`.
 */
class m181119_160900_create_post_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_image', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer(10),
            'image' => $this->string(255)
        ]);

        $this->createIndex('idx-post_id','post_image','post_id');

        $this->addForeignKey('fk-post_id',
            'post_image',
            'post_id',
            'post',
            'id',
            'CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_images');
    }
}
