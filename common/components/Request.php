<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 09.02.15
 * Time: 10:23
 */
namespace common\components;
use yii;
use yii\helpers\VarDumper;

class Request extends \yii\web\Request

{
    public $web;
    public $adminUrl;

    public function getBaseUrl()

    {
        return str_replace($this->web, "", parent::getBaseUrl()) . $this->adminUrl;

    }

    public function init()
    {
        $url = $this->getUrl();
        $matches = array();
        preg_match('/^\/([a-z]{2})\//', $url, $matches);
        if(is_array($matches))
        {
            if(!empty($matches))
            {
                Yii::$app->language = $matches[1];
                $this->setUrl(str_replace($matches[0], '/', $url));
            }
        }
        parent::init();
    }

    public function resolvePathInfo()
    {
        if ($this->getUrl() === $this->adminUrl) {
            return "";
        } else {
            return parent::resolvePathInfo();
        }
    }
}