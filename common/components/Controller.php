<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 11/13/17
 * Time: 9:51 AM
 */

namespace common\components;

use common\models\Currency;
use common\models\Language;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Cookie;

class Controller extends \yii\web\Controller
{
    public $languages;

    public function init()
    {
        parent::init();

        if(!Yii::$app->session->has('languages')) {
            $langs = Language::find()->select('code')->where(['enabled' => 1])->asArray()->all();
            foreach ($langs as $lang) {
                $_SESSION['languages'][] = $lang['code'];
            }
        }

        if(!isset($_SESSION['currencies'])) {
            $currencies = Currency::find()->select('code')->where(['is_active' => 1])->asArray()->column();
            foreach ($currencies as $currency) {
                $_SESSION['currencies'][] = $currency;
            }
        }
        // Change language by ip address
        if (empty($_SESSION['language'])) {
            $meta = unserialize(file_get_contents('http://ip-api.com/php/' . $_SERVER['REMOTE_ADDR']));
            switch ($meta['countryCode']) {
                case 'AZ':
                    $_SESSION['language'] = 'az';
                    break;
                case 'RU':
                    $_SESSION['language'] = 'ru';
                    break;
                case 'TR':
                    $_SESSION['language'] = 'tr';
                    break;
                default:
                    $_SESSION['language'] = 'en';
            }
        }

//        if (empty($_SESSION['language'])) {
//            $_SESSION['language'] = Yii::$app->params['defaultLanguage'];
//        }
        //set lang
        Yii::$app->language = $_SESSION['language'];

        if (!Yii::$app->session->has('currency')){
            $meta = unserialize(file_get_contents('http://ip-api.com/php/' . $_SERVER['REMOTE_ADDR']));
            switch ($meta['countryCode']) {
                case 'AZ':
                    $_SESSION['currency'] = 'AZN';
                    break;
                case 'RU':
                    $_SESSION['currency'] = 'RUB';
                    break;
                case 'TR':
                    $_SESSION['currency'] = 'TRY';
                    break;
                default:
                    $_SESSION['currency'] = 'USD';
            }
//            Yii::$app->session->set('currency', $_SESSION['currency']);
        }
    }

    public function beforeAction($action)
    {
        /*$session = Yii::$app->session;

        if ($action->id != 'set-lang' && $action->controller->id == 'media' && $action->id == 'index' ) {
            $session->set('route',$action->controller->id.'/'.$action->id);
            $session->set('slug',$_GET['slug']);
        }
        elseif($action->id != 'set-lang' && $action->controller->id == 'media' && $action->id == 'view'){
            $session->set('route',$action->controller->id.'/'.$action->id);
            $session->set('slug',$_GET['slug']);
            $session->set('slug1',$_GET['slug1']);
        }
        elseif($action->controller->id != 'site' and $action->id != 'set-lang' ){
            $session->remove('route');
        }
        elseif($action->controller->id == 'site' and $action->id == 'index' ){
            $session->remove('route');
        }*/

        return parent::beforeAction($action);
    }

}