<?php
namespace common\components\currency;
use Yii;

/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 6/17/17
 * Time: 11:34 PM
 */

class CurrencyHelper
{
    const CURRENCY_AZN = 'AZN';

    public static function roundRate($from, $to, $price)
    {
        if ($price && $from && $to) {
            return round(($price * $from) / $to,2);
        }
        return 0;
    }

    public static function icon($currency, $class='')
    {
        $currency = strtoupper($currency);

        $cssClass = 'icon-currency-'.strtolower($currency);

        return "<i class='$cssClass $class'></i>";
    }
}