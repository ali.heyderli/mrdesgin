<?php

namespace common\components\currency;
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 6/17/17
 * Time: 11:34 PM
 */
use Yii;
use \yii\web\Cookie;

class CurrencyRate
{
    private $rateObj = null;
    private $currency;
    private $rate;
    private $rates = array();

    public function __construct($currency)
    {
        $this->currency = $currency;
    }

    private function setClient()
    {

        $this->rateObj = new CbarRate($this->currency);
        $this->rates = (!empty($this->rateObj) ? $this->rateObj->getRates() : 1);
    }

    public function getRates()
    {
        $this->rates = $this->getRateFromState();
        if($this->rates) {
            return $this->rates;
        }
        $this->setClient();
        $this->setState();
        return $this->rates;
    }

    public function getRateFromState(){
        $currency = isset(Yii::$app->request->cookies['rates']) ? Yii::$app->request->cookies['rates']->value : '';
        return $currency;
    }

    public function setState() {
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => 'rates',
            'value' => $this->rates,
            'expire' => time() + 60*60,
        ]));

    }
}