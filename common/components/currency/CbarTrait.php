<?php

namespace common\components\currency;

/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 6/18/17
 * Time: 2:08 AM
 */
trait CbarTrait
{
    public function setRate($currency)
    {
        $newRate = array();
        $xml1 = @file_get_contents('http://www.cbar.az/currencies/'.date('d.m.Y').'.xml');
        if($xml1){
            $xml = new \SimpleXMLElement($xml1);
            /*$url = 'https://www.cbar.az/currencies/'.date('d.m.Y').'.xml';
            $xml = new \SimpleXMLElement($url,null,true);*/

            /** @noinspection PhpUndefinedFieldInspection */
            $rates = $xml->ValType->Type == 'Xarici valyutalar'
                ? $xml->ValType[0]->Valute
                : $xml->ValType[1]->Valute;



            foreach ($rates as $r)
            {
                foreach ($currency as $curr) {
                    if ($curr == $r->attributes()->Code) {
                        $newRate[$curr] = (string)$r->Value;
                    }
                }
            }
            $newRate['AZN'] = 1;
            $this->rate = $newRate;
        }

    }
}