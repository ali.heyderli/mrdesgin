<?php
namespace common\components\currency;

/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 6/18/17
 * Time: 1:25 AM
 */
interface CurrencyStrategy
{
    public function getRates();
}