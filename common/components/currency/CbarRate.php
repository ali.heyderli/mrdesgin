<?php

namespace common\components\currency;

/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 6/18/17
 * Time: 1:29 AM
 */

class CbarRate implements CurrencyStrategy
{
    use CbarTrait;

    private $currency;
    private $rate;

    public function __construct($currency)
    {
        $this->currency = $currency;
    }

    public function getRates()
    {
        $this->setRate($this->currency);
        return $this->rate;
    }
}