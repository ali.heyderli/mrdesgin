<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 11/13/17
 * Time: 9:51 AM
 */

namespace common\components;

use Yii;
use yii\helpers\VarDumper;

class UrlManager extends \yii\web\UrlManager
{
    public function createUrl($params)
    {
        $s = '';
        $params['lang'] = isset($params['lang']) ? $params['lang'] : Yii::$app->language;

        $s = '/'.$params['lang'];
        if (isset($params['lang'])) {
            unset($params['lang']);
        }
        if (isset($params[0]) && (strpos($params[0], 'images/') !== false || (strpos($params[0], '/uploads/') !== false))) {
            return parent::createUrl($params);
        }

        return $s.(parent::createUrl($params));
    }
}