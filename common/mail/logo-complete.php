<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 2018-12-03
 * Time: 14:24
 */
$offerLink = 'https://mrdesign.az/az/logo/view?id='.$model->id;
?>
<tr>
    <td style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
        <div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color: #646465;text-align: left;">
            <?=Yii::t('main','Sizin logo sifarişiniz hazırdır')?>
        </div>
        <br>
        <div class="body-text"
             style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
            <?=Yii::t('main','Dear')?> <?=$model->user->username;?>
            <p style="margin-bottom: 0;">
                <div>
                <?= Yii::t('main', 'Aşağıdakı linkdən sifarişinizə baxa bilərsiniz') ?>
            </div>
                <a style="width: 180px;text-align: center;padding: 14px 0;font-size: 18px;display: block;margin:auto;background: #555;color: #fff;text-decoration: none;border-radius: 2px;margin-top: 40px;" href="<?=$offerLink?>"><?=Yii::t('main','Bax')?></a>
            </p>
        </div>
    </td>
</tr>