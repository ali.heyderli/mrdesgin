<?php
return [
    'noreplyEmail' => 'info@mrdesign.az',
    'adminEmail' => 'info@mrdesign.az',
    'contactEmail' => 'info@mrdesign.az',
    'supportEmail' => 'info@mrdesign.az',
    'systemEmail' => 'mrdesignsistem@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration' => 3600 * 24 * 30,
    'defaultLanguage' => 'az',
    'bsVersion' => '3.x',
];
