<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

$rootPath = dirname(dirname(__DIR__)).'/frontend/web';

Yii::setAlias('@partnersRoot', $rootPath . '/uploads/partners');
Yii::setAlias('@partners', '/uploads/partners');

Yii::setAlias('@portfolioRoot', $rootPath . '/uploads/portfolio');
Yii::setAlias('@portfolio', '/uploads/portfolio');

Yii::setAlias('@postRoot', $rootPath . '/uploads/post');
Yii::setAlias('@post', '/uploads/post');
Yii::setAlias('@postThumbRoot', $rootPath . '/uploads/post/thumb');
Yii::setAlias('@postThumb', '/uploads/post/thumb');