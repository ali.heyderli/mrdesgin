<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sms_name".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 */
class SmsName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            [['id', 'user_id'], 'integer'],
            [['name'], 'string', 'max' => 11],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'name' => Yii::t('main', 'Name'),
            'user_id' => Yii::t('main', 'User ID'),
        ];
    }
}
