<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $description
 * @property string $category_id
 * @property int $due_date
 * @property int $completed
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'due_date', 'category_id'], 'required'],
            [['user_id', 'completed', 'created_at', 'updated_at', 'category_id'], 'integer'],
            [['description', 'due_date'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'title' => Yii::t('main', 'title'),
            'description' => Yii::t('main', 'description'),
            'due_date' => Yii::t('main', 'date'),
            'completed' => Yii::t('main', 'completed'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getImages()
    {
        return $this->hasMany(PostImage::className(), ['post_id' => 'id']);
    }

    public static function getPostCategories()
    {
        return [
            1 => Yii::t('main','Simple post'),
            2 => Yii::t('main','Creative post'),
            3 => Yii::t('main','Photo shot'),
            4 => Yii::t('main','Video shot'),
            5 => Yii::t('main','Animation'),

        ];
    }

    public static function getCategory($id)
    {
        if(key_exists($id, self::getPostCategories())) {
            return self::getPostCategories()[$id];
        }
    }

    public function getLimit($category_id)
    {
        $user_plan = Subscription::find()->where(['user_id' => Yii::$app->user->id, 'type' => 1,'status' => 1])->one();
        $posts_count = Post::find()->where(['user_id' => Yii::$app->user->id, 'category_id' => $category_id])->count();
        switch ($category_id){
            case 1:
                if($user_plan->plan->simple_post > $posts_count):
                    return true;
                endif;
                break;
            case 2:
                if($user_plan->plan->creative_post > $posts_count):
                    return true;
                endif;
                break;
            case 3:
                if($user_plan->plan->photo_shot > $posts_count):
                    return true;
                endif;
                break;
            case 4:
                if($user_plan->plan->video_shot > $posts_count):
                    return true;
                endif;
                break;
            case 5:
                if($user_plan->plan->animation > $posts_count):
                    return true;
                endif;
                break;
        }
       return false;
    }

    public function getComplete()
    {
        return $this->hasOne(PostComplate::className(), ['post_id' => 'id']);
    }
}
