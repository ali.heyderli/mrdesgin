<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $user_id
 * @property int $completed
 * @property int $created_at
 * @property int $updated_at
 * @property string $company_name
 * @property string $logo_name
 * @property string $slogan
 * @property string $about_company
 * @property string $colors
 * @property string $other_logos
 * @property string $company_desc
 * @property int $logo_type
 * @property int $type_1
 * @property int $type_2
 * @property int $type_3
 * @property int $type_4
 *
 * @property User $user
 * @property LogoImage[] $logoImages
 */
class Logo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'user_id', 'logo_name','company_name', 'about_company', 'company_desc'], 'required'],
            [['description', 'about_company'], 'string'],
            [['user_id', 'completed', 'created_at', 'updated_at', 'logo_type','type_1','type_2','type_4'], 'integer'],
            [['title', 'company_name', 'logo_name', 'slogan', 'colors', 'other_logos', 'company_desc'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'title' => Yii::t('main', 'Title'),
            'description' => Yii::t('main', 'Description'),
            'user_id' => Yii::t('main', 'User ID'),
            'completed' => Yii::t('main', 'Completed'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
            'company_name' => Yii::t('main', 'Company name'),
            'logo_name' => Yii::t('main', 'Logo Name'),
            'slogan' => Yii::t('main', 'Slogan'),
            'about_company' => Yii::t('main', 'About Company'),
            'colors' => Yii::t('main', 'Colors'),
            'other_logos' => Yii::t('main', 'Other Logos'),
            'company_desc' => Yii::t('main', 'Company Desc'),
            'logo_type' => Yii::t('main', 'Logo Type'),
            'type_1' => Yii::t('main', 'Logo form'),
            'type_2' => Yii::t('main', 'Logo form'),
            'type_3' => Yii::t('main', 'Logo form'),
            'type_4' => Yii::t('main', 'Logo form'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogoImages()
    {
        return $this->hasMany(LogoImage::className(), ['logo_id' => 'id']);
    }

    public static function getForms()
    {
        return [
            0 => 'Neytral',
            1 => 'Klassik',
            2 => 'Müasir',
            3 => 'Neytral',
            4 => 'Zərif',
            5 => 'Sərt',
            6 => 'Neytral',
            7 => 'Abstrakt',
            8 => 'Hərfi',
            9 => 'Neytral',
            10 => 'Əyləncəli',
            11 => 'Ciddi',

        ];
    }

    public static function getForm($id) {
        return self::getForms()[$id];
    }

    public static function getTypes()
    {
        return  [0=>'Abstrakt', 1 => 'Emblem', 2 => 'Hərf',
            3=>'Kombinasiya', 4 => 'Maskot', 5 => 'Piktorial', 6 => 'Söz'] ;
    }

    public function getComplete()
    {
        return $this->hasOne(LogoComplete::className(), ['logo_id' => 'id']);
    }

}
