<?php

namespace common\models;

use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "post_category".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $enabled
 *
 * @property PostCategoryLang[] $postCategoryLangs
 */
class PostCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'enabled'], 'integer'],
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => $_SESSION['languages'],
                'languageField' => 'lang',
                'defaultLanguage' => $_SESSION['language'] ? $_SESSION['language'] : 'az',
                'langForeignKey' => 'category_id',
                'tableName' => "{{%post_category_lang}}",
                'attributes' => [
                    'name'
                ]
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
            'enabled' => Yii::t('main', 'Enabled'),
        ];
    }


    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

}
