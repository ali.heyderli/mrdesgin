<?php

namespace common\models;

use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "sms_plan".
 *
 * @property int $id
 * @property int $price
 * @property int $created_at
 * @property int $is_active
 * @property int $updated_at
 *
 * @property SmsPlanLang[] $smsPlanLangs
 */
class PricePlan extends \yii\db\ActiveRecord
{
    const POST_TYPE = 1;
    const LOGO_TYPE = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price'], 'required'],
            [['price', 'created_at', 'is_active', 'updated_at', 'type', 'count'], 'integer'],
            [['name', 'text'],'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'price' => Yii::t('main', 'Price'),
            'count' => Yii::t('main', 'Count'),
            'created_at' => Yii::t('main', 'Created At'),
            'is_active' => Yii::t('main', 'Is Active'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => $_SESSION['languages'],
                'languageField' => 'lang',
                'defaultLanguage' => $_SESSION['language'],
                'langForeignKey' => 'price_plan_id',
                'tableName' => "{{%price_plan_lang}}",
                'attributes' => [
                    'name', 'text'
                ]
            ],
            TimestampBehavior::className(),
        ];
    }
    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */

}
