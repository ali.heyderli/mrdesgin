<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logo_image".
 *
 * @property int $id
 * @property int $logo_id
 * @property string $image
 *
 * @property Logo $logo
 */
class LogoImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['logo_id', 'image'], 'required'],
            [['logo_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['logo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Logo::className(), 'targetAttribute' => ['logo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'logo_id' => Yii::t('main', 'Logo ID'),
            'image' => Yii::t('main', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Logo::className(), ['id' => 'logo_id']);
    }
}
