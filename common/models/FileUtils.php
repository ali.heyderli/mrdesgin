<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 12/5/18
 * Time: 11:10 AM
 */

namespace common\models;



class FileUtils
{
    const PRODUCT_THUMP_SIZE = 400;
    const MEDIA_THUMP_SIZE = 300;
    const MEDIA_THUMP2_SIZE = 300;
    const USER_THUMB_SIZE = 150;
    /**
     * @return array file types
     */
    public static function getTypes()
    {
        return [
            'application/pdf'   => '.pdf',
            'application/zip'   => '.zip',
            'image/gif'         => '.gif',
            'image/jpeg'        => '.jpg',
            'image/png'         => '.png',
            'text/css'          => '.css',
            'text/html'         => '.html',
            'text/javascript'   => '.js',
            'text/plain'        => '.txt',
            'text/xml'          => '.xml',
        ];
    }

    /**
     * @param string $mimeType
     * @return array file type
     */
    public static function getType($mimeType)
    {
        if (in_array($mimeType, array_keys(static::getTypes()))) {
            return static::getTypes()[$mimeType];
        }
        return null;
    }
}