<?php

namespace common\models;

use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "pcategory".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $enabled
 *
 * @property PcategoryLang[] $pcategoryLangs
 */
class Pcategory extends \yii\db\ActiveRecord
{
    const ENABLED = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pcategory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'enabled'], 'integer'],
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => $_SESSION['languages'] ? $_SESSION['languages'] : ['en','az','ru'] ,
                'languageField' => 'lang',
                'defaultLanguage' => $_SESSION['language'] ? $_SESSION['language'] : 'az',
                'langForeignKey' => 'pcategory_id',
                'tableName' => "{{%pcategory_lang}}",
                'attributes' => [
                    'name'
                ]
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
            'enabled' => Yii::t('main', 'Enabled'),
        ];
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */

}
