<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sms_campaigne".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $target_id
 * @property int $sender_id
 * @property int $created_at
 * @property int $quantity
 *
 * @property User $user
 * @property Target $target
 * @property SmsName $sender
 */
class SmsCampaigne extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_campaigne';
    }

    public function behaviors()
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['target_id', 'sender_id', 'quantity', 'text'], 'required'],
            [['user_id', 'target_id', 'sender_id', 'created_at', 'quantity'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 160],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['target_id'], 'exist', 'skipOnError' => true, 'targetClass' => Target::className(), 'targetAttribute' => ['target_id' => 'id']],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsName::className(), 'targetAttribute' => ['sender_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'name' => Yii::t('main', 'Name'),
            'user_id' => Yii::t('main', 'User ID'),
            'target_id' => Yii::t('main', 'Target ID'),
            'sender_id' => Yii::t('main', 'Sender ID'),
            'text' => Yii::t('main', 'Text'),
            'created_at' => Yii::t('main', 'Created At'),
            'quantity' => Yii::t('main', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(Target::className(), ['id' => 'target_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(SmsName::className(), ['id' => 'sender_id']);
    }
}
