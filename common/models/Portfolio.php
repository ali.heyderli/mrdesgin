<?php

namespace common\models;

use mihaildev\fileupload\FileUploadBehavior;
use mohorev\file\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\imagine\Image;

/**
 * This is the model class for table "portfolio".
 *
 * @property int $id
 * @property int $pcategory_id
 * @property string $name
 * @property string $image_f
 * @property string $image_b
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Pcategory $pcategory
 */
class Portfolio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portfolio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pcategory_id', 'name'], 'required'],
            [['pcategory_id', 'created_at', 'updated_at'], 'integer'],
            [['image_f',], 'required', 'on' => ['insert']],
            [['image_f'], 'safe', 'on' => ['insert','update']],
            [['name'], 'string', 'max' => 255],
            [['image_f', 'image_b'], 'safe'],
            [['pcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pcategory::className(), 'targetAttribute' => ['pcategory_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image_f',
                'scenarios' => ['insert', 'update'],
                'path' => Yii::getAlias('@portfolioRoot'),
                'url' => Yii::getAlias('@portfolio'),
            ],
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'pcategory_id' => Yii::t('main', 'Pcategory ID'),
            'name' => Yii::t('main', 'Name'),
            'image_f' => Yii::t('main', 'Image Front'),
            'image_b' => Yii::t('main', 'Image Back'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcategory()
    {
        return $this->hasOne(Pcategory::className(), ['id' => 'pcategory_id']);
    }
}
