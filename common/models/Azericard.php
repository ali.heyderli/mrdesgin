<?php
namespace common\models;
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 2019-01-27
 * Time: 14:36
 */

class Azericard
{
    private $terminal = '17201541';
    private $key_for_sign = '53bf1923e896001cbbc52d6122d9f7f1';
    private $url = 'https://mpi.3dsecure.az/cgi-bin/cgi_link';
    private $test_url = 'https://testmpi.3dsecure.az/cgi-bin/cgi_link';
    private $test_url2 = 'https://213.172.75.248/cgi-bin/cgi_link';


    public function auth($order_id,$amount)
    {
        // These fields will be change
        $db_row['AMOUNT'] = $amount;
        $db_row['CURRENCY'] = 'AZN';
        $db_row['ORDER'] = $order_id;

        // These fields will be always static
        $db_row['DESC'] = 'New Subscription';
        $db_row['MERCH_NAME'] = 'Mrdesgin.az';
        $db_row['MERCH_URL'] = 'https://mrdesign.az';
        $db_row['TERMINAL'] = $this->terminal;		// That is your personal ID in payment system
        $db_row['EMAIL'] = 'info@mrdesign.az';
        $db_row['TRTYPE'] = '1';					// That is the type of operation, 1 - Authorization and checkout
        $db_row['COUNTRY'] = 'AZ';
        $db_row['MERCH_GMT'] = '+4';
        $db_row['BACKREF'] = 'https://mrdesign.az/order/callback';
        //These fields are generated automaticaly every request
        $oper_time=gmdate("YmdHis");			// Date and time UTC
        $nonce=substr(md5(rand()),0,16);		// Random data
        $to_sign = "".strlen($db_row['AMOUNT']).$db_row['AMOUNT']
            .strlen($db_row['CURRENCY']).$db_row['CURRENCY']
            .strlen($db_row['ORDER']).$db_row['ORDER']
            .strlen($db_row['DESC']).$db_row['DESC']
            .strlen($db_row['MERCH_NAME']).$db_row['MERCH_NAME']
            .strlen($db_row['MERCH_URL']).$db_row['MERCH_URL']
            .strlen($db_row['TERMINAL']).$db_row['TERMINAL']
            .strlen($db_row['EMAIL']).$db_row['EMAIL']
            .strlen($db_row['TRTYPE']).$db_row['TRTYPE']
            .strlen($db_row['COUNTRY']).$db_row['COUNTRY']
            .strlen($db_row['MERCH_GMT']).$db_row['MERCH_GMT']
            .strlen($oper_time).$oper_time
            .strlen($nonce).$nonce
            .strlen($db_row['BACKREF']).$db_row['BACKREF'];

        $key_for_sign = $this->key_for_sign; 				// Key for sign will change in production system
        $p_sign=hash_hmac('sha1',$to_sign, $this->yubi_hex2bin($key_for_sign));
        $data = array(
            'AMOUNT' => $db_row['AMOUNT'],
            'CURRENCY' => $db_row['CURRENCY'],
            'ORDER' => $db_row['ORDER'],
            'DESC' => $db_row['DESC'],
            'MERCH_NAME' => $db_row['MERCH_NAME'],
            'MERCH_URL' => $db_row['MERCH_URL'],
            'TERMINAL' => $db_row['TERMINAL'],
            'EMAIL' => $db_row['EMAIL'],
            'TRTYPE' => $db_row['TRTYPE'],
            'COUNTRY' => $db_row['COUNTRY'],
            'MERCH_GMT' => $db_row['MERCH_GMT'],
            'TIMESTAMP' => $oper_time,
            'NONCE' => $nonce,
            'BACKREF' => $db_row['BACKREF'],
            'LANG' => 'AZ',
            'P_SIGN' => $p_sign,
        );
        return $this->submitForm($data);
    }

    public function submitForm($data)
    {
        if ($data) {
            $form = "<form id='azericard' action='". $this->url."' method='post'>";
            $form .= "<input name=\"AMOUNT\" value=\"{$data['AMOUNT']}\" type=\"hidden\">
            <input name=\"CURRENCY\" value=\"{$data['CURRENCY']}\" type=\"hidden\">
            <input name=\"ORDER\" value=\"{$data['ORDER']}\" type=\"hidden\">
            <input name=\"DESC\" value=\"{$data['DESC']}\" type=\"hidden\">
            <input name=\"MERCH_NAME\" value=\"{$data['MERCH_NAME']}\" type=\"hidden\">
            <input name=\"MERCH_URL\" value=\"{$data['MERCH_URL']}\" type=\"hidden\">
            <input name=\"TERMINAL\" value=\"{$data['TERMINAL']}\" type=\"hidden\">
            <input name=\"EMAIL\" value=\"{$data['EMAIL']}\" type=\"hidden\">
            <input name=\"TRTYPE\" value=\"{$data['TRTYPE']}\" type=\"hidden\">
            <input name=\"COUNTRY\" value=\"{$data['COUNTRY']}\" type=\"hidden\">
            <input name=\"MERCH_GMT\" value=\"{$data['MERCH_GMT']}\" type=\"hidden\">
            <input name=\"TIMESTAMP\" value=\"{$data['TIMESTAMP']}\" type=\"hidden\">
            <input name=\"NONCE\" value=\"{$data['NONCE']}\" type=\"hidden\">
            <input name=\"BACKREF\" value=\"{$data['BACKREF']}\" type=\"hidden\">
            <input name=\"LANG\" value=\"AZ\" type=\"hidden\">
            <input name=\"P_SIGN\" value=\"{$data['P_SIGN']}\" type=\"hidden\">
            ";
            $form.="<script type=\"text/javascript\"> document.forms['azericard'].submit();</script>";
            echo $form;
        }
    }

    public function process($data)
    {
        $oper_time = gmdate("YmdHis");
        $nonce=substr(md5(rand()),0,16);
        $Post_Data["AMOUNT"] = $data["AMOUNT"];
        $Post_Data["CURRENCY"] = $data['CURRENCY'];
        $Post_Data["ORDER"] = $data["ORDER"];
        $Post_Data["RRN"] = $data["RRN"];				// Bank's reference number
        $Post_Data["INT_REF"] = $data["INT_REF"];
        $Post_Data["TERMINAL"] = $data["TERMINAL"];
        $Post_Data["TRTYPE"] = "21"; 					 // TRTYPE=21 - Checkout
        $Post_Data["TIMESTAMP"] = $oper_time;
        $Post_Data["NONCE"] = $nonce;
        $to_sign = "".strlen($data['ORDER']).$data['ORDER'].
            strlen($data['AMOUNT']).$data['AMOUNT'].
            strlen($data['CURRENCY']).$data['CURRENCY'].
            strlen($data['RRN']).$data['RRN'].
            strlen($data['INTREF']).$data['INTREF'].
            strlen("21")."21".
            strlen($data['TERMINAL']).$data['TERMINAL'].
            strlen($oper_time).$oper_time.
            strlen($nonce).$nonce;

        $key_for_sign = $this->key_for_sign;    // Key for sign will change in production system
        $p_sign=hash_hmac('sha1',$to_sign, $this->yubi_hex2bin($key_for_sign));

        $Post_Data["P_SIGN"] = $p_sign;

        foreach($Post_Data as $key => $value){
            $Post[] = "$key=$value";
        }
        $Post = implode("&",$Post);
        return $result = $this->get_web_page($this->url,$Post);

    }

    public function get_web_page( $url, $data_in ){
        $options = array(
            //CURLOPT_SSLVERSION     => 3,
            CURLOPT_RETURNTRANSFER => true,		// return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            //CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            //-------to post-------------
            CURLOPT_POST		   => true,
            CURLOPT_POSTFIELDS	   => $data_in,	//$data,
            CURLOPT_SSL_VERIFYPEER => false,    // DONT VERIFY
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_CAINFO		   => "a.cer",
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );
        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;

        return $header;
    }

    public function yubi_hex2bin($hexdata) {
        $bindata="";
        for ($i=0;$i<strlen($hexdata);$i+=2) {
            $bindata.=chr(hexdec(substr($hexdata,$i,2)));
        }
        return $bindata;
    }
}
