<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "logo_plan".
 *
 * @property int $id
 * @property double $price
 * @property double $plan_name
 * @property string $plan
 * @property int $created_at
 * @property int $updated_at
 */
class LogoPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'plan',], 'required'],
            [['price'], 'number'],
            [['plan', 'plan_name'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    public function behaviors()
    {
       return [
           TimestampBehavior::className()
       ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'price' => Yii::t('main', 'Price'),
            'plan' => Yii::t('main', 'Plan'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }
}
