<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "number_base".
 *
 * @property int $id
 * @property int $target_id
 * @property int $number
 * @property int $sent
 */
class NumberBase extends \yii\db\ActiveRecord
{
    public $url;
    public $quantity;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'number_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['target_id', 'number'], 'required'],
            [['target_id', 'quantity', 'sent'], 'integer'],
            [['number'], 'string'],
            [['url'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'target_id' => Yii::t('main', 'Target ID'),
            'number' => Yii::t('main', 'Number'),
            'sent' => Yii::t('main', 'Sent'),
        ];
    }
}
