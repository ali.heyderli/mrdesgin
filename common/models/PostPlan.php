<?php

namespace common\models;

use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "post_plan".
 *
 * @property int $id
 * @property string $plan_name
 * @property int $simple_post
 * @property int $creative_post
 * @property string $photo_shot
 * @property string $video_shot
 * @property string $animation
 * @property string $created_at
 * @property string $updated_at
 * @property string $price
 */
class PostPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_name', 'simple_post', 'creative_post', 'photo_shot', 'video_shot', 'animation', 'price'], 'required'],
            [['simple_post', 'creative_post', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['photo_shot', 'video_shot', 'animation'], 'string'],
            [['plan_name'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => $_SESSION['languages'],
                'languageField' => 'lang',
                'defaultLanguage' => $_SESSION['language'],
                'langForeignKey' => 'post_plan_id',
                'tableName' => "{{%post_plan_lang}}",
                'attributes' => [
                    'name'
                ]
            ],
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'plan_name' => Yii::t('main', 'Plan Name'),
            'simple_post' => Yii::t('main', 'Simple Post'),
            'creative_post' => Yii::t('main', 'Creative Post'),
            'photo_shot' => Yii::t('main', 'Photo Shot'),
            'video_shot' => Yii::t('main', 'Video Shot'),
            'animation' => Yii::t('main', 'Animation'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }
}
