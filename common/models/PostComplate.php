<?php

namespace common\models;

use mohorev\file\UploadBehavior;
use Yii;

/**
 * This is the model class for table "post_complate".
 *
 * @property int $id
 * @property int $post_id
 * @property string $image
 * @property string $caption
 */
class PostComplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_complate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_id', 'image'], 'required'],
            [['post_id'], 'integer'],
            [['caption'], 'string'],
            [['image'], 'safe'],
            [['image'], 'required', 'on' => ['insert']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image',
                'scenarios' => ['insert', 'update'],
                'path' => Yii::getAlias('@postRoot'),
                'url' => Yii::getAlias('@post'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'post_id' => Yii::t('main', 'Post ID'),
            'image' => Yii::t('main', 'File'),
            'caption' => Yii::t('main', 'Caption'),
        ];
    }
}
