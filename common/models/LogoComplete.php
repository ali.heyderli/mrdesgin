<?php

namespace common\models;

use mohorev\file\UploadBehavior;
use Yii;

/**
 * This is the model class for table "logo_complete".
 *
 * @property int $id
 * @property int $logo_id
 * @property string $file
 * @property string $text
 * @property int $status
 */
class LogoComplete extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo_complete';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['logo_id', 'file'], 'required'],
            [['logo_id', 'status'], 'integer'],
            [['text'], 'string'],
            [['file'], 'safe'],
            [['file'], 'required', 'on' => ['insert']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'scenarios' => ['insert', 'update'],
                'path' => Yii::getAlias('@postRoot'),
                'url' => Yii::getAlias('@post'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'logo_id' => Yii::t('main', 'Logo ID'),
            'file' => Yii::t('main', 'File'),
            'text' => Yii::t('main', 'Text'),
            'status' => Yii::t('main', 'Status'),
        ];
    }
}
