<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "subscription".
 *
 * @property int $id
 * @property int $user_id
 * @property int $price_plan_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $type
 *
 * @property PricePlan $pricePlan
 * @property User $user
 */
class Subscription extends \yii\db\ActiveRecord
{
    const POST_TYPE = 1;
    const LOGO_TYPE = 2;
    const SMM_TYPE = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'price_plan_id', 'type'], 'required'],
            [['RRN', 'INTREF'], 'string'],
            [['user_id', 'price_plan_id', 'type', 'created_at', 'updated_at','status','paid'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'price_plan_id' => Yii::t('main', 'Price Plan ID'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getPlan()
    {
        if($this->type == self::POST_TYPE) {
            return $this->hasOne(PostPlan::className(), ['id' => 'price_plan_id']);
        }
        else if($this->type == self::LOGO_TYPE ) {
            return $this->hasOne(LogoPlan::className(), ['id' => 'price_plan_id']);
        }
        else if($this->type == self::SMM_TYPE ) {
            return $this->hasOne(SmmPlan::className(), ['id' => 'price_plan_id']);
        }
        return false;
    }
}
