<?php
namespace frontend\controllers;

use common\models\NumberBase;
use common\models\SmsCampaigne;
use common\models\SmsName;
use common\models\Subscription;
use common\models\Target;
use common\models\User;
use frontend\models\SendSmsForm;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\components\Controller;

class AppController extends Controller
{
    /**
     * {@inheritdoc}
     */

     public function Behaviors()
     {
       return [
           'access' => [
               'class' => AccessControl::className(),
               'only' => ['index','create-post'],
               'rules' => [
                   [
                       'actions' => ['index'],
                       'allow' => true,
                       'roles' => ['viewUserPage'],
                   ],
                   [
                       'actions' => ['create-post'],
                       'allow' => true,
                       'roles' => ['viewPostPage'],
                   ],
               ],
           ],
       ];
     }


     //App home page
     public function actionIndex()
     {
         $subscriptions = Subscription::find()->where(['user_id' => Yii::$app->user->id,'status' => 1])->all();
         $model = User::findOne(Yii::$app->user->id);
         if(Yii::$app->request->isPost) {
             $model->company = $_POST['User']['company'];
             $model->phone = $_POST['User']['phone'];
             if($model->validate()) {
                 $model->save();
             }
         }
       return $this->render('index', [
           'subscriptions' => $subscriptions,
           'model' => $model
       ]);
     }

     //Create post

    public function actionCreatePost()
    {

    }

}
