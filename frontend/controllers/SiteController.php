<?php
namespace frontend\controllers;

use common\models\Azericard;
use common\models\LogoPlan;
use common\models\Partners;
use common\models\Pcategory;
use common\models\Portfolio;
use common\models\PostPlan;
use common\models\PricePlan;
use common\models\SmmPlan;
use common\models\Subscription;
use backend\models\User;
use frontend\models\CallForm;
use phpDocumentor\Reflection\Types\Void_;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Exception;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\components\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'subscribe',],
                'rules' => [
                    [
                        'actions' => ['signup',],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['subscribe'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout','app'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
            ],
        ];
    }
    protected function verbs()
    {
        return [
            'order-callback' => ['POST'],
        ];
    }

    public function beforeAction($action)
    {
        if ($this->action->id == 'order-callback') {
            $this->enableCsrfValidation = false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $partners = Partners::find()->all();
        $portfolio_logo = Portfolio::find()->where(['pcategory_id' => 4])->limit(10)->all();
        $portfolio_post = Portfolio::find()->where(['pcategory_id' => 5])->limit(10)->all();
        $portfolio_animation = Portfolio::find()->where(['pcategory_id' => 6])->limit(10)->all();
        $price_plans = PricePlan::find()->where(['is_active' => 1])->all();
        $p_categories = Pcategory::find()->all();
        return $this->render('index', [
            'partners' => $partners,
            'portfolios_logo' => $portfolio_logo,
            'portfolios_post' => $portfolio_post,
            'portfolio_animation' => $portfolio_animation,
            'price_plans' => $price_plans,
            'p_categories' => $p_categories
        ]);
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            VarDumper::dump($model->sendEmail(Yii::$app->params['adminEmail']),6,1);die;
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('main','contact_success'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('main','contact_error'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {

        $partners = Partners::find()->all();
        return $this->render('about', [
            'partners' => $partners
        ]);
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    public function actionSetLang() {
        $langCodes = $_SESSION['languages'];
        $language = Yii::$app->request->get('language', false);
        $url = Yii::$app->request->referrer;

        if($language && in_array($language, $langCodes)) {

            Yii::$app->language = $language;
            $_SESSION['language'] = $language;

            $urlArray = explode('/', $url);

            if(count($urlArray) > 3) {
                if(!isset($urlArray[4])) {
                    $urlArray[4] = '';
                }

                $urlArray[3] = $language;
                $url = implode('/', $urlArray);
                return $this->redirect($url);
            }
        }

        return $this->redirect($url);
    }

    public function actionSetCurrency()
    {
        $currency = Yii::$app->request->get('currency', false);
        $url = Yii::$app->request->referrer;
        if($currency) {
            Yii::$app->session['currency'] = $currency;
        }
        return $this->redirect($url);
    }

    /**si
     * Subscribe page
     *
     * @return mixed
     */
    public function actionSubscribe($id, $type)
    {
        if(Yii::$app->user->isGuest) {
            return $this->redirect(['/login']);
        }
        $check = Subscription::find()->where(['user_id' => Yii::$app->user->id , 'type' => $type,'status' => 1])->one();
        if($check){
            return $this->redirect(Url::to(['app/index']));
        }
        $plan_id = filter_var(intval($id), FILTER_SANITIZE_NUMBER_INT);
        switch ($type) {
            case Subscription::POST_TYPE:
                $plan = PostPlan::find()->where(['id' => $plan_id])->one();
                break;
            case Subscription::LOGO_TYPE:
                $plan = LogoPlan::find()->where(['id' => $plan_id])->one();
                break;
            case Subscription::SMM_TYPE:
                $plan = SmmPlan::find()->where(['id' => $plan_id])->one();
                break;
        }
        $model = new Subscription();
        $model->user_id = Yii::$app->user->id;
        $model->price_plan_id = $plan_id;
        $model->type = $type;
        if($model->save()) {
            $payment = new Azericard();
            return $payment->auth($model->id,$plan->price);
        }
    }

    public function actionOrderCallback()
    {
        if($_POST["ACTION"] == "0") {
            $payment = new Azericard();
            $data = array(
                'AMOUNT' => $_POST['AMOUNT'],
                'CURRENCY' => $_POST['CURRENCY'],
                'ORDER' => $_POST['ORDER'],
                'TERMINAL' => $_POST['TERMINAL'],
                'RRN' => $_POST['RRN'],
                'INT_REF' => $_POST['INT_REF'],
            );
            $subscription = Subscription::find()->where(['id' => $_POST['ORDER']])->one();
            $subscription->paid = 1;
            $subscription->status = 1;
            $subscription->RRN = $_POST['RRN'];
            $subscription->INTREF = $_POST['INT_REF'];
            $subscription->save(false);
            $payment->process($data);
            $auth = \Yii::$app->authManager;
            if ($subscription->type == 1) {
                $authorRole = $auth->getRole('postSubscribe');
            }
            elseif ($subscription->type == 2) {
                $authorRole = $auth->getRole('logoSubscribe');
            }
            elseif ($subscription->type == 3) {
                $authorRole = $auth->getRole('smmSubscribe');
            }
            //assign new role
            $auth->assign($authorRole, $subscription->user_id);
            //TODO set flash message
            return $this->redirect(Url::to(['app/index']));
        }
        else {
            return $this->redirect(Url::to(['site/index']));
        }

    }

    public function actionCallBack()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new CallForm();
        if( Yii::$app->request->isPost) {
            if($model->load(Yii::$app->request->post())) {
                $model->sendEmail();
                return [
                    'success' => 1,
                    'message' => Yii::t('main', 'call_message')
                ];
            }
            else {
                return [
                    'success' => 0,
                    'message' => Yii::t('main', 'call_error_message')
                ];
            }

        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionTestPayment()
    {
        return $this->render('payment');
    }

}
