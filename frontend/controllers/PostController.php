<?php

namespace frontend\controllers;

use common\components\currency\CurrencyRate;
use common\models\FileUtils;
use common\models\PostComplate;
use common\models\PostImage;
use common\models\PostPlan;
use common\models\PricePlan;
use common\models\User;
use Yii;
use common\models\Post;
use common\models\PostSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use common\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public $rates;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [''],
                'rules' => [
                    [
                        'actions' => ['index','create', 'view', 'delete', 'update', 'beyenmedim'],
                        'allow' => true,
                        'roles' => ['viewPostPage'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewPostPage')) {
            return $this->render('not-subscribed');
        }
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $posts = Post::find()->where(['user_id' => Yii::$app->user->id])->all();
        $user = User::findOne(Yii::$app->user->id);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'posts' => $posts,
            'user' => $user
        ]);
    }

    public function actionIndex2()
    {
        $price_plans = PostPlan::find()->all();
        $rate = new CurrencyRate($_SESSION['currencies']);
        $this->rates = $rate->getRates();
        return $this->render('index2', [
            'price_plans' => $price_plans,
            'rate' => $this->rates,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewPostPage')) {
            return $this->render('not-subscribed');
        }
        $completed = PostComplate::find()->where(['post_id' => $id])->one();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'completed' => $completed
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('viewPostPage')) {
            return $this->render('not-subscribed');
        }
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        $model = new Post();
        $posts = Post::find()->where(['user_id' => Yii::$app->user->id])->count();
        if ($model->load(Yii::$app->request->post())) {
            if($model->getLimit($model->category_id)) {
                $model->user_id = Yii::$app->user->id;
                $model->due_date = strtotime($model->due_date);
                $model->completed = 0;
//            VarDumper::dump($model->save(false),6,1);die;

                if($model->save(false)) {
                    if (!empty($_FILES['Post']['tmp_name']['images'])) {
                        foreach ($_FILES['Post']['tmp_name']['images'] as $key => $image) {
                            if (!empty($image)) {
                                $ext = FileUtils::getType($_FILES['Post']['type']['images'][$key]);
                                $imageName = substr(sha1(time() . rand(1000, 9999)), 0, 12) . '.' . $ext;
                                $mainImg = Yii::getAlias('@postRoot/') . $imageName;
                                $imgInfo = getimagesize($image);
                                $mainThumbImg = Yii::getAlias('@postThumbRoot/') . $imageName;

                                Image::thumbnail($image, $imgInfo[0], $imgInfo[1])->save($mainImg, ['quality' => 90]);
                                Image::thumbnail(
                                    $image,
                                    FileUtils::PRODUCT_THUMP_SIZE,
                                    FileUtils::PRODUCT_THUMP_SIZE)
                                    ->save($mainThumbImg, ['quality' => 90]);
                                $otherImg = new PostImage();
                                $otherImg->post_id = $model->id;
                                $otherImg->image = $imageName;
                                $otherImg->save();
                            }
                        }
                    }
                    $params = [
                        'model' => $model,
                    ];
                    Yii::$app->mailer->compose(['html' => 'new-order'],$params)
                        ->setTo(Yii::$app->params['systemEmail'])
                        ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                        ->setSubject('Yeni sifarish')
                        ->send();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            else {
                return $this->render('limit');
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('viewPostPage')) {
            return $this->render('not-subscribed');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('viewPostPage')) {
            return $this->render('not-subscribed');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }


    public function actionBeyenmedim($id)
    {
        if (!Yii::$app->user->can('viewPostPage')) {
            return $this->render('not-subscribed');
        }
        $model = $this->findModel($id);
        $model->completed = 2;
        $model->save(false);
        $params = [
            'model' => $model,
        ];
        Yii::$app->mailer->compose(['html' => 'post-beyenme'],$params)
            ->setTo(Yii::$app->params['systemEmail'])
            ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
            ->setSubject('Post bəyənilmədi')
            ->send();

        return $this->redirect(['index']);
    }
}
