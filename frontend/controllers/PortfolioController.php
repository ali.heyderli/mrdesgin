<?php

namespace frontend\controllers;

use common\components\Controller;
use common\models\Pcategory;
use common\models\Portfolio;

class PortfolioController extends Controller
{
    public function actionIndex()
    {
        $portfolio_logo = Portfolio::find()->where(['pcategory_id' => 4])->orderBy(['created_at' => SORT_DESC])->all();
        $portfolio_post = Portfolio::find()->where(['pcategory_id' => 5])->all();
        $portfolio_animation = Portfolio::find()->where(['pcategory_id' => 6])->all();
        $p_categories = Pcategory::find()->all();
        return $this->render('index', [
            'portfolios_logo' => $portfolio_logo,
            'portfolios_post' => $portfolio_post,
            'portfolios_animation' => $portfolio_animation,
            'p_categories' => $p_categories
        ]);
    }
}

