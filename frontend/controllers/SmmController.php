<?php
namespace frontend\controllers;

use common\models\NumberBase;
use common\models\SmmPlan;
use common\models\SmsCampaigne;
use common\models\SmsName;
use common\models\Target;
use frontend\models\SendSmsForm;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\components\Controller;

class SmmController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function Behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [ ''],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['viewSmmPage'],
                    ],
                ],
            ],
        ];
    }
    //App home page
    public function actionIndex()
    {

        $smm_plans = SmmPlan::find()->all();
        return $this->render('index2', [
            'price_plans' => $smm_plans
        ]);
    }

    //Create post

    public function actionAppPage()
    {
        if (!Yii::$app->user->can('viewSmmPage')) {
            return $this->render('not-subscribed');
        }
        return $this->render('index');

    }

}
