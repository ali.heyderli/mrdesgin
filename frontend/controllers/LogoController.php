<?php

namespace frontend\controllers;

use common\components\currency\CurrencyRate;
use common\models\LogoPlan;
use common\models\PricePlan;
use Yii;
use common\models\Logo;
use common\models\LogoSearch;
use yii\filters\AccessControl;
use common\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogoController implements the CRUD actions for Logo model.
 */
class LogoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $rates;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [''],
                'rules' => [
                    [
                        'actions' => ['index','create', 'view', 'delete', 'update', 'beyenmedim'],
                        'allow' => true,
                        'roles' => ['viewLogoPage'],
                    ],
                ],
            ],
                        'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'beyenmedim' => ['POST'],
                ],
            ],

//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
        ];
    }

    /**
     * Lists all Logo models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewLogoPage')) {
            return $this->render('not-subscribed');
        }
        $searchModel = new LogoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->id]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex2()
    {
        $price_plans = LogoPlan::find()->all();
        $rate = new CurrencyRate($_SESSION['currencies']);
        $this->rates = $rate->getRates();
        return $this->render('index2', [
            'price_plans' => $price_plans,
            'rate' => $this->rates,
        ]);
    }

    /**
     * Displays a single Logo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewLogoPage')) {
            return $this->render('not-subscribed');
        }
        $model = Logo::find()->where(['id' => $id, 'user_id' => Yii::$app->user->id])->one();
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Logo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('viewLogoPage')) {
            return $this->render('not-subscribed');
        }
        $model = new Logo();
        if(Yii::$app->user->identity->logoCount >= 1) {
            return $this->render('limit');
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            if ($model->save()){
                $params = [
                    'model' => $model,
                ];
                Yii::$app->mailer->compose(['html' => 'new-order'],$params)
                    ->setTo(Yii::$app->params['systemEmail'])
                    ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
                    ->setSubject('Yeni sifarish')
                    ->send();
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Logo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('viewLogoPage')) {
            return $this->render('not-subscribed');
        }
        $model = Logo::find()->where(['id' => $id, 'user_id' => Yii::$app->user->id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Logo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('viewLogoPage')) {
            return $this->render('not-subscribed');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Logo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logo::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }

    public function actionBeyenmedim($id)
    {
        if (!Yii::$app->user->can('viewLogoPage')) {
            return $this->render('not-subscribed');
        }
        $model = $this->findModel($id);
        $model->completed = 2;
        $model->save();
        $params = [
            'model' => $model,
        ];
        Yii::$app->mailer->compose(['html' => 'logo-beyenme'],$params)
            ->setTo(Yii::$app->params['systemEmail'])
            ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
            ->setSubject('Logo bəyənilmədi')
            ->send();
        return $this->redirect(['index']);
    }
}
