<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;


$this->title = $name;
?>
<section class="height-100 dark-bg last-section">
    <div id="particle-canvas" data-dot-color="#00c3da" data-line-color="#2f2f2f"><canvas class="pg-canvas" width="1440" height="511" style="display: block;"></canvas></div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">
                <div class="error-page">
                    <div class="title">
                        <h1 class="colored-text"><?=$exception->statusCode?></h1>
                        <h2><?=$this->title?></h2>
                        <h4><?=$exception->getMessage()?></h4>
                    </div><a class="btn btn-color" href="<?=Url::to(Yii::$app->request->referrer)?>">Geri</a>
                </div>
            </div>
        </div>
    </div>
</section>