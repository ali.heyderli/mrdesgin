<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('main','about_us');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-40.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">
                <div class="title text-center">
                    <h4 class="upper"></h4>
                    <h1><?=$this->title?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="padding-top: 50px">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title text-center">
                    <h2></h2>
                </div>
                <div class="section-content text-center">
                    <p class="fw-300 font-20">
                        <?=Yii::t('main','about_text')?>
                    </p>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="split-section">
    <div class="side-background">
        <div class="col-md-6 col-sm-4 img-side img-right">
            <div class="img-holder">
                <img class="bg-img" src="/layout/images/bg-image-41.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-sm-7">
            <ul class="nav nav-tabs minimal-tabs cols-3">
                <li class="active"><a href="#tab-1" data-toggle="tab"><h2><?=Yii::t('main','mission_header')?></h2></a>
                </li>
            </ul>
            <div class="tab-content pt-25 pb-25">
                <div class="tab-pane fade active in" id="tab-1">
                    <p><?=Yii::t('main','mission')?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="split-section">
    <div class="side-background">
        <div class="col-md-6 col-sm-4 img-side img-left">
            <div class="img-holder">
                <img class="bg-img" src="/layout/images/bg-image-42.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-sm-7 col-md-offset-7 col-sm-offset-5">
            <div class="title">
                <h2><?=Yii::t('main','our_skils')?></h2>
            </div>
            <div class="section-content">
                <div class="skill">
                    <h5 class="skill-name"><?=Yii::t('main','branding')?><span class="skill-perc">90%</span></h5>
                    <div class="progress">
                        <div class="progress-bar" style="background-color:rgb(212, 76, 88)" role="progressbar" data-progress="90"></div>
                    </div>
                </div>
                <div class="skill">
                    <h5 class="skill-name"><?=Yii::t('main','logo')?><span class="skill-perc">80%</span></h5>
                    <div class="progress">
                        <div class="progress-bar" style="background-color:rgb(212, 76, 88)" role="progressbar" data-progress="80"></div>
                    </div>
                </div>
                <div class="skill">
                    <h5 class="skill-name"><?=Yii::t('main','smm')?><span class="skill-perc">95%</span></h5>
                    <div class="progress">
                        <div class="progress-bar" style="background-color:rgb(212, 76, 88)" role="progressbar" data-progress="95"></div>
                    </div>
                </div>
                <div class="skill">
                    <h5 class="skill-name"><?=Yii::t('main','video')?><span class="skill-perc">85%</span></h5>
                    <div class="progress">
                        <div class="progress-bar" style="background-color:rgb(212, 76, 88)" role="progressbar" data-progress="85"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="client-carousel carousel" data-slick="{&quot;slidesToShow&quot;: 6, &quot;dots&quot;: false}">
                <?php foreach ($partners as $partner):?>
                    <div class="carousel-item">
                        <figure>
                            <img src="<?= Yii::getAlias('@partners').'/'.$partner->image ?>" alt="">
                        </figure>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>