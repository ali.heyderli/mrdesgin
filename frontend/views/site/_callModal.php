<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 2018-12-06
 * Time: 10:46
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$model = new \frontend\models\CallForm();
?>
<div class="modal fade" id="callModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="margin-top: 80px; padding-right: 0">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modalLabel"><?= Yii::t('main', 'contact') ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success alert-dismissible" id="call-alert" role="alert" style="display: none">
                    <div class="alert-icon"><i class="hc-checkmark-circle"></i>
                    </div>
                    <p id="alert-call"></p>
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><i class="hc-close"></i>
                    </button>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'callForm']); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => Yii::t('main','name')])->label(false) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => Yii::t('main','email')])->label(false) ?>

                <?= $form->field($model, 'subject')->textInput(['maxlength' => true, 'placeholder' => Yii::t('main','subject')])->label(false) ?>
                <?= $form->field($model, 'message')->textarea(['maxlength' => true, 'placeholder' => Yii::t('main','message')])->label(false) ?>

<!--                --><?//= $form->field($model, 'phone')->textInput(['placeholder' => Yii::t('main','phone')])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('main', 'send'), ['class' => 'btn  btn-color', 'id' => 'submit-call']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-backdrop {
        z-index: 1;
    }
</style>
