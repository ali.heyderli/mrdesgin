<?php
$this->title = 'Logopob.com';

use yii\helpers\Url;
?>

<section class="section-skewed">
    <div class="container">
        <div class="title text-center">
            <h2><?=Yii::t('main','services')?></h2>
        </div>
        <div class="section-content pt-25 pb-25">
            <div class="row">
                <div class="col-md-4">
                    <a href="<?= Url::to(['/posts']) ?>">
                    <div class="icon-box boxed-style align-center" data-animation="zoomOut">
                        <i class="hc-brush" style="color: rgb(212, 76, 88);"></i>
                        <div class="ib-content">
                            <h4><?= Yii::t('main','posts') ?></h4>
                            <p><?= Yii::t('main','smm_post_text') ?></p>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?= Url::to(['/logo']) ?>">
                    <div class="icon-box boxed-style align-center" data-animation="zoomOut">
                        <i class="hc-pencil-ruler" style="color: rgb(212, 76, 88);" ></i>
                        <div class="ib-content">
                            <h4><?= Yii::t('main','logo') ?></h4>
                            <p><?= Yii::t('main','logo_home_text') ?></p>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?= Url::to(['/smm-page']) ?>">
                    <div class="icon-box boxed-style align-center" data-animation="zoomOut">
                        <i class="hc-videocam" style="color: rgb(212, 76, 88);"></i>
                        <div class="ib-content">
                            <h4><?= Yii::t('main','animation_title') ?></h4>
                            <p><?= Yii::t('main','animation_text') ?></p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gradient-bg section-skewed big-padding-bt" data-gradients="#d6505c,#d54b57">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title txt-sm-center">
                    <h4 class="upper"></h4>
                    <h2><?=Yii::t('main','latest_works')?></h2>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-1">
                <ul class="mt-50" id="filters">
                    <?php foreach ($p_categories as $p_category):?>
                        <li <?= ($p_category->id == 4) ? 'class="active"' : ""?>data-filter=".<?=$p_category->id?>"><?=$p_category->name?></li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="grey-bg section-skewed pt-0">
    <div class="container">
        <div class="row" data-negative-margin="150">
            <div class="enable-animation six-col with-spacing" id="works-grid">
                <?php foreach($portfolios_logo as $portfolio_logo):?>
                    <div class="work-item <?=$portfolio_logo->pcategory->id?>">
                        <div class="work-detail">
                            <a href="<?=Yii::getAlias('@portfolio').'/'.$portfolio_logo->image_f?>" data-lightbox="<?=$portfolio_logo->image_f?>">
                                <img src="<?=Yii::getAlias('@portfolio').'/'.$portfolio_logo->image_f?>" alt="">
                                <!--                                <div class="work-info">-->
                                <!--                                    <div class="centrize">-->
                                <!--                                        <div class="v-center">-->
                                <!--                                            <h3>Mux Cards</h3>-->
                                <!--                                            <p>Branding, UI/UX</p>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
                <?php foreach($portfolios_post as $portfolio_post):?>
                    <div class="work-item  <?=$portfolio_post->pcategory->id?>">
                        <div class="work-detail">
                            <a href="<?=Yii::getAlias('@portfolio').'/'.$portfolio_post->image_f?>" data-lightbox="<?=$portfolio_post->image_f?>">
                                <img src="<?=Yii::getAlias('@portfolio').'/'.$portfolio_post->image_f?>" alt="">
                                <!--                                <div class="work-info">-->
                                <!--                                    <div class="centrize">-->
                                <!--                                        <div class="v-center">-->
                                <!--                                            <h3>Mux Cards</h3>-->
                                <!--                                            <p>Branding, UI/UX</p>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
                <?php foreach($portfolio_animation as $portfolio_ani):?>
                    <div class="work-item  <?=$portfolio_ani->pcategory->id?>">
                        <div class="work-detail">
                            <a href="<?=Yii::getAlias('@portfolio').'/'.$portfolio_ani->image_f?>" data-lightbox="<?=$portfolio_ani->image_f?>">
                                <img src="<?=Yii::getAlias('@portfolio').'/'.$portfolio_ani->image_f?>" alt="">
                                <!--                                <div class="work-info">-->
                                <!--                                    <div class="centrize">-->
                                <!--                                        <div class="v-center">-->
                                <!--                                            <h3>Mux Cards</h3>-->
                                <!--                                            <p>Branding, UI/UX</p>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>

<section class=" section-skewed pt-100 pb-100">
    <div class="container">
        <div class="title text-center">
            <h2><?=Yii::t('main','partners')?></h2>
        </div>
        <div class="row">
            <div class="client-carousel carousel" data-slick="{&quot;slidesToShow&quot;: 6, &quot;dots&quot;: false}">
                <?php foreach ($partners as $partner):?>
                    <div class="carousel-item">
                        <figure>
                            <img src="<?= Yii::getAlias('@partners').'/'.$partner->image ?>" alt="">
                        </figure>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
<section class="parallax-section">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-3.jpg" alt="">
            </div>
            <div class="parallax-overlay gradient-overlay" data-gradients="#6a6a6a,#6a6a6a"></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="title text-center">
                    <h3 style="font-size: 21px"><?=Yii::t('main','call_back_text')?></h3>
                    <p class="fw-300"><?= Yii::t('main','call_back_text2'); ?></p>
                </div>
                <div class="section-content">
                    <p class="text-center m-0"><button class="btn btn-light"  data-toggle="modal" data-target="#callModal"><?=Yii::t('main','contact')?></button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->render('@frontend/views/site/_callModal')?>
<style>
    .post {
        display: none;
        color: #FFEB3B;
    }
    .section-skewed h2 {
        color: #000 !important;
    }
</style>
