<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form',
                'fieldConfig' => [
                    /*'errorOptions' => ['class' => 'md-input-danger'],*/
                    'options' => [
                        'tag' => false,
                    ],
                ],]); ?>
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <h1><?= Html::encode($this->title) ?></h1>
                        <p class="text-muted">Please choose your new password:</p>
                        <div class="input-group mb-3 field-resetpasswordform-email">
                            <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-envelope"></i>
                    </span>
                            </div>
                            <?= $form->field($model, 'email')->passwordInput(['autofocus' => true,'placeholder' => Yii::t('main','new_password')])->label(false) ?>


                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <button class="btn btn-link px-0"
                                        type="button">
                                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>