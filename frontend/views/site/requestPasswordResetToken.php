<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form',
                'fieldConfig' => [
                    /*'errorOptions' => ['class' => 'md-input-danger'],*/
                    'options' => [
                        'tag' => false,
                    ],
                ],]); ?>
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <h1><?= Html::encode($this->title) ?></h1>
                        <p class="text-muted">Please fill out your email. A link to reset password will be sent there.</p>
                        <div class="input-group mb-3 field-passwordresetrequestform-email">
                            <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-envelope"></i>
                    </span>
                            </div>
                            <?= $form->field($model, 'email')->textInput(['autofocus' => true,'placeholder' => Yii::t('main','email')])->label(false) ?>


                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <button class="btn btn-link px-0"
                                        type="button">
                                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
