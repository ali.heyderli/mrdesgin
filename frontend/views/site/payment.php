<form ACTION="https://213.172.75.248/cgi-bin/cgi_link" METHOD="POST">

    <tbody>
    <tr>
        <td colspan="3" valign="top"><center><font size="4">Reversal form.</font></center></td>
    </tr>
    <tr>
        <td colspan="3" valign="top"><font size="3"></font></td>
    </tr>
    <tr bgcolor="#808080">
        <td colspan="3" valign="top">Reversal Details</td>
    </tr>

    <?php

    function yubi_hex2bin($hexdata) {
        $bindata="";

        for ($i=0;$i<strlen($hexdata);$i+=2) {
            $bindata.=chr(hexdec(substr($hexdata,$i,2)));
        }

        return $bindata;
    }

    // Getting required fields

    // These fields must be equal such fields getting from authorization responce
    $db_row['AMOUNT'] = '499';
    $db_row['CURRENCY'] = 'AZN';
    $db_row['ORDER'] = '100021';
    $db_row['RRN'] = '902876721535';			// Bank reference number
    $db_row['INT_REF'] = 'AC93F4414E8B112D';		// Internal reference number


    // These fields shouldn't change anytime
    $db_row['TERMINAL'] = '17200024';			// That is your personal ID in payment system
    $db_row['TRTYPE'] = '22';					// That is the type of operation, 22 - Reversal


    // Fields generated automatically
    $oper_time=gmdate("YmdHis");			// Date and time UTC
    $nonce=substr(md5(rand()),0,16);		// Random data

    // ------------------------------

    foreach($db_row as $key => $value){
        echo "<tr><td>$key"." = "."$value</td></tr>";
        #echo "<input name=\"$key\" value=\"$value\" type=\"hidden\">";
    }

    // Creating form hidden fields

    echo "
	<input name=\"AMOUNT\" value=\"{$db_row['AMOUNT']}\" type=\"hidden\">
    <input name=\"CURRENCY\" value=\"{$db_row['CURRENCY']}\" type=\"hidden\">
	<input name=\"ORDER\" value=\"{$db_row['ORDER']}\" type=\"hidden\">
	<input name=\"RRN\" value=\"{$db_row['RRN']}\" type=\"hidden\">
	<input name=\"INT_REF\" value=\"{$db_row['INT_REF']}\" type=\"hidden\">	
    <input name=\"TERMINAL\" value=\"{$db_row['TERMINAL']}\" type=\"hidden\">
    <input name=\"TRTYPE\" value=\"{$db_row['TRTYPE']}\" type=\"hidden\">    
	<input name=\"TIMESTAMP\" value=\"$oper_time\" type=\"hidden\">
	<input name=\"NONCE\" value=\"$nonce\" type=\"hidden\">
	";

    // ------------------------------------------------

    // Making P_SIGN (MAC)	-         Checksum of request
    // All following fields must be equal with hidden fields above

    $to_sign = "".strlen($db_row['ORDER']).$db_row['ORDER']
        .strlen($db_row['AMOUNT']).$db_row['AMOUNT']
        .strlen($db_row['CURRENCY']).$db_row['CURRENCY']
        .strlen($db_row['RRN']).$db_row['RRN']
        .strlen($db_row['INT_REF']).$db_row['INT_REF']
        .strlen($db_row['TRTYPE']).$db_row['TRTYPE']
        .strlen($db_row['TERMINAL']).$db_row['TERMINAL']
        .strlen($oper_time).$oper_time
        .strlen($nonce).$nonce;

    $key_for_sign="c393d14f3ac995c95b792780d0b29434";			// Key for sign will change in production system
    $p_sign=hash_hmac('sha1',$to_sign, yubi_hex2bin($key_for_sign));

    echo "<input name=\"P_SIGN\" value=\"$p_sign\" type=\"hidden\">";
    // ----------------------------------------------------
    ?>

    <table border="0" cellpadding="5" cellspacing="5" width="590" align="center">
        <input alt="Submit" type="submit">
        </tbody></table>
</form>	