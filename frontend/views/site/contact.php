<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::t('main','contact');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">
                <div class="title text-center">
                    <h4 class="upper"></h4>
                    <h1><?=Yii::t('main','contact')?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="p-0">
    <div class="container-fluid">
        <div class="row row-flex">
            <div class="col-sm-4">
                <div class="column-inner with-padding">
                    <div class="icon-box align-center"><i class="hc-pin"></i>
                        <div class="ib-content">
                            <h3><?=Yii::t('main','address')?></h3>
                            <p>Caspian plaza</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="column-inner with-padding grey-bg">
                    <div class="icon-box align-center"><i class="hc-mail-open"></i>
                        <div class="ib-content">
                            <h3><?=Yii::t('main','email_us')?></h3>
                            <p><a href="mailto:info@mrdesign.az">info@mrdesign.az</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="column-inner with-padding dark-bg">
                    <div class="icon-box align-center"><i class="hc-megaphone"></i>
                        <div class="ib-content">
                            <h3><?=Yii::t('main','contact_us')?></h3>
                            <p>+994 70 283-40-00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

