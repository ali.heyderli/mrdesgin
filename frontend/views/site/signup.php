<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:200px;padding-top: 90px">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">
                <div class="title text-center">
                    <h4 class="upper"></h4>
                    <h1><?=$this->title?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="col-md-6 col-md-offset-3">
                <div class="icon-box-app">
                    <?php $form = ActiveForm::begin(['id' => 'form-signup',
                        'fieldConfig' => [
                            /*'errorOptions' => ['class' => 'md-input-danger'],*/
                            'options' => [
                                'tag' => false,
                            ],
                        ],]); ?>
                    <div class="card-group">
                        <div class="card p-4">
                            <div class="card-body">
                                <h3><?= Html::encode($this->title) ?></h3>
                                <p class="text-muted">Please fill out the following fields to signup:</p>
                                    <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username', 'autofocus' => true])->label(false) ?>
                                    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
                                    <?= $form->field($model, 'phone')->textInput(['placeholder' => Yii::t('main', 'phone')])->label(false) ?>
                                    <?= $form->field($model, 'company')->textInput(['placeholder' => Yii::t('main', 'company')])->label(false) ?>
                                    <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false) ?>
                                    <?= Html::submitButton(Yii::t('main','signup'), ['class' => 'btn btn-color btn-sm', 'name' => 'signup-button']) ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</section>


