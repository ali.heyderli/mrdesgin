<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('main', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:200px;padding-top: 90px">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">
                <div class="title text-center">
                    <h4 class="upper"></h4>
                    <h1><?=$this->title?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
                <div class="col-md-6 col-md-offset-3">
                    <div class="icon-box-app">
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                        <div class="card-group">
                            <div class="card p-4">
                                <div class="card-body">
                                    <h3><?=Yii::t('main','login')?></h3>
                                    <p class="text-muted"><?=Yii::t('main','sign_in_your_account')?></p>
                                    <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('main','username')])->label(false) ?>

                                    <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('main','password')])->label(false) ?>
                                    <?= Html::submitButton(Yii::t('main','login'), ['class' => 'btn btn-color btn-sm', 'name' => 'login-button']) ?>
                                    <br>
                                    <br>
                                    <?= Html::a(Yii::t('main','forgot_pass'), ['site/request-password-reset']) ?>
                                    <br>
                                    <?= Html::a(Yii::t('main','registration'), ['/signup']) ?>


                                </div>
                            </div>

                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
        </div>
    </div>
</section>
