<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('main','Send bulk sms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row justify-content-center">
              <div class="col-sm-12 col-md-6">
                <div class="card">
                  <div class="card-header"><strong><?=$this->title?></strong></div>
                  <div class="card-body">
                      <?php if(Yii::$app->session->hasFlash('bulk-sms')){ ?>
                          <div class="alert alert-danger" role="alert"><?=Yii::$app->session->getFlash('bulk-sms')?></div>
                      <?php } ?>
                      <?php $form = ActiveForm::begin(['id' => 'smscampaigne','action' => \yii\helpers\Url::to(['/app/send-bulk-sms']),
                          'fieldConfig' => [
                              /*'errorOptions' => ['class' => 'md-input-danger'],*/
                              'options' => [
                                  'tag' => false,
                              ],
                          ],]); ?>
                      <div class="form-group field-smscampaigne-target_id">
                      <?= $form->field($model, 'target_id')->dropDownList($targetNames,['prompt' => 'Select target']) ?>
                      </div>
                      <div class="form-group field-smscampaigne-sender_id">
                          <?= $form->field($model, 'sender_id')->dropDownList($smsNames,['prompt' => 'Select sender name']) ?>
                      </div>
                      <div class="form-group field-smscampaigne-quantity">
                          <?= $form->field($model, 'quantity')->textInput(['placeholder' => 'Only number']) ?>
                      </div>
                      <!--<div class="form-group field-smscampaigne-date " >
                          <label>Send date</label>
                          <div class="input-group date" id="datetimepicker1" data-target-input="nearest">

                              <input type="text" class="form-control datetimepicker-input" name="SmsCampaigne[created_at]" data-target="#datetimepicker1"/>
                              <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                          </div>
                      </div>-->
                      <div class="form-group field-smscampaigne-text">
                          <?= $form->field($model, 'text')->textarea(['rows' => '5','placeholder' => 'Sms text']) ?>
                      </div>
                      <?= Html::submitButton(Yii::t('main','Send'), ['class' => 'btn btn-primary px-4', 'name' => 'send-button']) ?>

                      <?php ActiveForm::end(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
