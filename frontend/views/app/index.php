<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('main','profile');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li class="active"><a href="<?= Url::to(['app/index']) ?>"><?= Yii::t('main','profile') ?></a></li>
                            <li class=""><a href="<?= Url::to(['post/index']) ?>"><?= Yii::t('main','order_post') ?></a></li>
                            <li><a href="<?= Url::to(['logo/index']) ?>"><?= Yii::t('main','order_logo') ?></a></li>
                            <li><a href="<?= Url::to(['/smm']) ?>"><?= Yii::t('main','smm') ?></a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?=Yii::t('main','logout')?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <div style="margin-bottom: 20px">
                            <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                            <strong><?=$this->title?></strong>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12 col-md-12">
                                <div class="card">
                                </div>
                                <div class="card-body">
                                    <div class="col-sm-12">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#home"><?= Yii::t('main','profile') ?></a></li>
                                            <li><a data-toggle="tab" href="#messages"><?= Yii::t('main','subscriptions') ?></a></li>
                                        </ul>


                                        <div class="tab-content">
                                            <div class="tab-pane active" id="home">
                                                <hr>
                                                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                                                <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username', 'disabled' => true])->label(false) ?>
                                                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email', 'disabled' => true])->label(false) ?>
                                                <?= $form->field($model, 'phone')->textInput(['placeholder' => Yii::t('main', 'phone')])->label(false) ?>
                                                <?= $form->field($model, 'company')->textInput(['placeholder' => Yii::t('main', 'company')])->label(false) ?>
                                                <?= Html::submitButton(Yii::t('main','update'), ['class' => 'btn btn-color btn-sm', 'name' => 'signup-button']) ?>
                                                <?php ActiveForm::end(); ?>

                                                <hr>

                                            </div><!--/tab-pane-->
                                            <div class="tab-pane" id="messages">

                                                <h2></h2>

                                                <hr>
                                                <?php if($subscriptions):?>
                                                    <table class="table table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col"><?= Yii::t('main','plan'); ?></th>
                                                            <th scope="col"><?= Yii::t('main','subscription_date')?></th>
                                                            <th scope="col"><?= Yii::t('main','end_date')?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($subscriptions as $s):?>
                                                            <tr>
                                                                <th scope="row"><?=$s->id?></th>
                                                                <td><?= (isset($s->plan->plan_name)) ? $s->plan->plan_name : $s->plan->name; ?></td>
                                                                <td><?= date('Y-m-d', $s->created_at) ?></td>
                                                                <td><?= date("Y-m-d", strtotime("+1 month", $s->created_at))?></td>
                                                            </tr>
                                                        <?php endforeach;?>
                                                        </tbody>
                                                    </table>
                                                <?php endif;?>

                                            </div><!--/tab-pane-->

                                        </div><!--/tab-pane-->
                                    </div><!--/tab-content-->
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>