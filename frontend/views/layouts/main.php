<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 7/20/18
 * Time: 2:51 PM
 */

use frontend\assets\AppAsset;
use yii\helpers\Url;
use yii\helpers\Html;
AppAsset::register($this);

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/layout/images/fav.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/layout/css/bundle.css">
    <link rel="stylesheet" href="/layout/css/style.css?v=1.0.6.5">
    <link rel="stylesheet" href="/layout/css/icomoon.css?v=1.0.01">
    <link rel="stylesheet" href="/layout/css/hody-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.min.css">

    <?php $this->registerCsrfMetaTags() ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142725570-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-142725570-1');
    </script>

</head>


<body>
<?php $this->beginBody() ?>
<div id="wrapper">
<?= \frontend\widgets\HeaderWidget\HeaderWidget::widget()?>
<?= $content ?>
<?= \frontend\widgets\FooterWidget\FooterWidget::widget()?>
</div>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5bf7163d79ed6453ccaaafd5/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<script type="text/javascript" src="/layout/js/jquery.js"></script>
<script type="text/javascript" src="/layout/js/bundle.js"></script>
<script type="text/javascript" src="/layout/js/SmoothScroll.js"></script>
<script type="text/javascript" src="/layout/js/jquery.mb.YTPlayer.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNGOsBBZo9vf0Tw4w6aJiilSTFVfQ5GPI"></script>
<script type="text/javascript" src="/layout/js/input-mask.js"></script>
<script type="text/javascript" src="/layout/js/main.js?v=1.0.3"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js"></script>
<?php $this->endBody() ?>

</body>

</html>
<?php $this->endPage() ?>
