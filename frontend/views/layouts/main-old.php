<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <?php $this->beginBody() ?>
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="<?=Url::to(['/app'])?>">
        <span style="color: #2f353a">Mr</span>design
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav ml-auto">

        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <i style="font-size: 20px;" class="fa fa-user-circle" aria-hidden="true"></i>
              <?=Yii::$app->user->identity->username?>

          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Account</strong>
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-user"></i> Profile</a>
              <?= Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                '<i class="fa fa-lock"></i> Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'dropdown-item logout']
            )
            . Html::endForm()
              ?>

          </div>
        </li>
          <li class="nav-item d-md-down-none">

          </li>
      </ul>

    </header>
    <div class="app-body">
      <div class="sidebar">
        <nav class="sidebar-nav">
          <?php
          echo Nav::widget([
              'options' => ['class' => 'nav'],
              'encodeLabels' => false,
              'items' => [
                  ['label' => '<i class="nav-icon icon-home icons"></i> Sayta qayıt', 'url' => ['/'], 'options'=>['class'=>'nav-item'],'linkOptions'=>['class'=>'nav-link']],
                  ['label' => '<i class="nav-icon icon-speedometer"></i> Panel', 'url' => ['/app/index'], 'options'=>['class'=>'nav-item'],'linkOptions'=>['class'=>'nav-link']],
                  ['label' => '<i class="nav-icon fa fa-file-image-o"></i> Post', 'url' => ['/post/index'], 'options'=>['class'=>'nav-item'],'linkOptions'=>['class'=>'nav-link']],
                  ['label' => '<i class="nav-icon icon-grid icons"></i> Loqo', 'url' => ['/logo/index'], 'options'=>['class'=>'nav-item'],'linkOptions'=>['class'=>'nav-link']],
                  ['label' => '<i class="nav-icon fa fa-line-chart "></i> SMM', 'url' => ['/smm'], 'options'=>['class'=>'nav-item'],'linkOptions'=>['class'=>'nav-link']],

              ],
          ]);
          ?>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>
      <main class="main" style="padding-top: 30px">
        <!-- Breadcrumb-->



            <?=$content; ?>
            </main>

    </div>
    <footer class="app-footer">
      <div>
        <?=Yii::$app->name?> <?=Yii::$app->getVersion()?>
        <span>&copy; 2018</span>
      </div>
      <div class="ml-auto">
        <span>Created by</span>
        <a target="_blank" href="Paradesign.az">Paradesign</a>
      </div>
    </footer>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
