<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LogoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'logo');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li><a href="<?= Url::to(['app/index']) ?>"><?= Yii::t('main','profile') ?></a></li>
                            <li class=""><a href="<?= Url::to(['post/index']) ?>"><?= Yii::t('main','order_post') ?></a></li>
                            <li class="active"><a href="<?= Url::to(['logo/index']) ?>"><?= Yii::t('main','order_logo') ?></a></li>
                            <li><a href="<?= Url::to(['/smm']) ?>"><?= Yii::t('main','smm') ?></a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?=Yii::t('main','logout')?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <div style="margin-bottom: 20px">
                            <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                            <strong><?=$this->title?></strong>
                            <?= Html::a(Yii::t('main', 'create_logo'), ['create'], ['class' => 'btn btn-color btn-sm pull-right']) ?>

                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12 col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <?php Pjax::begin(); ?>
                                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                                        <?= GridView::widget([
                                            'dataProvider' => $dataProvider,
//                                            'filterModel' => $searchModel,
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                'company_name',
                                                'logo_name',
                                                [
                                                    'attribute' => 'completed',
                                                    'value' => function($data) {
                                                        switch ($data->completed) {
                                                            case 1:
                                                                $status = 'Hazırdır';
                                                                break;
                                                            case 2:
                                                                $status = 'Bəyənilmədi';
                                                                break;
                                                            default:
                                                                $status =  "Hazır deyil";
                                                                break;

                                                        }

                                                        return $status;
                                                    }
                                                ],

                                                //'created_at',
                                                //'updated_at',

                                                ['class' => 'yii\grid\ActionColumn',
                                                    'template' => '{view} ',
                                                    'buttons' => [
                                                        'view' => function($url, $model, $key) {
                                                            $html = "<a href='".$url."' class=''><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>";
                                                            return $html;
                                                        },

                                                    ]
                                                ],
                                            ],
                                        ]); ?>
                                        <?php Pjax::end(); ?>
                                    </div>
                                </div>
                            </div>
<!--                            <div class="col-sm-12 col-md-4">-->
<!--                                <div class="card">-->
<!--                                    <div class="card-header"><strong>--><?//=Yii::$app->user->identity->username?><!--</strong>-->
<!--                                    </div>-->
<!--                                    <div class="card-body">-->
<!--                                        <table class="table table-responsive-sm table-striped">-->
<!--                                            <tbody>-->
<!--                                            <tr>-->
<!--                                                <td>Aktiv logo planı</td>-->
<!--                                                <td><span class="badge badge-success">--><?//= Yii::$app->user->identity->logoSubscribe->plan->plan_name?><!--</span></td>-->
<!--                                            </tr>-->
<!--                                            --><?php //$features = explode(',', Yii::$app->user->identity->logoSubscribe->plan->plan) ?>
<!--                                            --><?php //foreach ($features as $feature): ?>
<!--                                                <tr>-->
<!--                                                    <td> </td>-->
<!--                                                    <td><span>--><?//= $feature?><!--</span></td>-->
<!--                                                </tr>-->
<!--                                            --><?php //endforeach; ?>
<!--                                            <tr>-->
<!--                                                <td>Etibarlıdır :</td>-->
<!--                                                <td><span class="badge badge-success">--><?//= date("Y-m-d", strtotime("+1 month", Yii::$app->user->identity->logoSubscribe->created_at))?><!--</span></td>-->
<!--                                            </tr>-->
<!--                                            </tbody>-->
<!--                                        </table>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
