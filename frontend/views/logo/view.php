<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */

$this->title = $model->logo_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Logos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li><a href="<?= Url::to(['app/index']) ?>"><?= Yii::t('main','profile') ?></a></li>
                            <li class=""><a href="<?= Url::to(['post/index']) ?>"><?= Yii::t('main','order_post') ?></a></li>
                            <li class="active"><a href="<?= Url::to(['logo/index']) ?>"><?= Yii::t('main','order_logo') ?></a></li>
                            <li><a href="<?= Url::to(['/smm']) ?>"><?= Yii::t('main','smm') ?></a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?=Yii::t('main','logout')?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <div style="margin-bottom: 20px;">
                            <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                            <strong><?=$this->title?></strong>
                            <?= Html::a(Yii::t('main', 'create_logo'), ['create'], ['class' => 'btn btn-color btn-sm']) ?>
                            <!--                            --><?//= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
<!--                            --><?//= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
//                                'class' => 'btn btn-danger btn-sm',
//                                'data' => [
//                                    'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
//                                    'method' => 'post',
//                                ],
//                            ]) ?>
                            <a style="float: right;" href="<?= Url::to(Yii::$app->request->referrer)?>"><i class="fa fa-undo"></i> Geri</a>
                        </div>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [


                                [
                                    'attribute' => 'completed',
                                    'value' => function($data) {
                                        switch ($data->completed) {
                                            case 1:
                                                $status = 'Hazırdır';
                                                break;
                                            case 2:
                                                $status = 'Bəyənilmədi';
                                                break;
                                            default:
                                                $status =  "Hazır deyil";
                                                break;

                                        }

                                        return $status;
                                    }
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'value' => function($data) {
                                        return  $data->created_at ? date('Y-m-d', $data->created_at) : '';
                                    }
                                ],

                                'company_name',
                                'logo_name',
                                'slogan',
                                'about_company:ntext',
                                'colors',
                                'other_logos',
                                'company_desc',
                                [
                                    'attribute' => 'logo_type',
                                    'value' => function($data) {
                                        return  \common\models\Logo::getTypes()[$data->logo_type];
                                    }
                                ],
                                [
                                    'attribute' => 'type_1',
                                    'value' => function($data) {
                                        return  \common\models\Logo::getForms()[$data->type_1];
                                    }
                                ],
                                [
                                    'attribute' => 'type_2',
                                    'value' => function($data) {
                                        return  \common\models\Logo::getForms()[$data->type_2];
                                    }
                                ],
                                [
                                    'attribute' => 'type_3',
                                    'value' => function($data) {
                                        return  \common\models\Logo::getForms()[$data->type_3];
                                    }
                                ],
                                [
                                    'attribute' => 'type_4',
                                    'value' => function($data) {
                                        return  \common\models\Logo::getForms()[$data->type_4];
                                    }
                                ]
                            ],
                        ]) ?>
                        <div style="margin-bottom: 20px;">
                            <h4>Hazırlanmış logo</h4>
                            <?php if($model->complete): ?>
                               <a class="btn btn-color btn-sm" target="_blank" href="<?= Yii::getAlias('@post').'/'.$model->complete->file ?>">Yüklə</a>
                                <?= Html::a(Yii::t('main', 'Bəyənmədim'), ['beyenmedim', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
//                                    'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                            <div>
                                <h4>Hazırlanmış logo haqqında qeydlər</h4>
                                <?= $model->complete->text ?>
                            </div>
                            <?php else:?>
                                <span>Sifarişiniz hazır olduqda buradan əlçatan olacaq</span>
                            <?php endif;?>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>
