<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logo-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'logo_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'slogan')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'colors')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'other_logos')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'about_company')->textarea(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'company_desc')->textarea(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12">
        <label style="color: #1f1f1f;
    font-family: 'Comfortaa', sans-serif;
    font-weight: 500;
    font-size: 13px;"><?=Yii::t('main','choose_logo_type')?></label>
        <div style="border: 2px solid #eee; position:relative; display: inline-block;">
        <?=
        $form->field($model, 'logo_type')
            ->radioList(
               [0=>'abstrakt', 1 => 'emblem', 2 => 'hərf',
            3=>'kombinasiya', 4 => 'maskot', 5 => 'piktorial', 6 => 'söz'] ,
                [
                    'item' => function($index, $label, $name, $checked, $value) {

                        $return = '<div class="col-md-4"> <label style="padding: 15px" class="modal-radio">';
                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                        $return .= '<img style= "width: 50px; margin: 0 10px 0 5px;" src="/app/img/logo/'.$label.'.jpg">';
                        $return .= '<span>' . ucwords($label) . '</span>';
                        $return .= '</label></div>';

                        return $return;
                    }
                ]
            )
            ->label(false);
        ?>
        </div>
    </div>
    </div>
    <div class="row logo_type" >
            <label style="color: #1f1f1f;
    font-family: 'Comfortaa', sans-serif;
    font-weight: 500;
    font-size: 13px;"><?=Yii::t('main','choose_logo_feature')?></label>

        <div class="col-md-12">
            <div class="logo-style">
            <?=
            $form->field($model, 'type_1')
                ->radioList([1 => 'Klassik', 2 => 'Müasir',0=>'Neytral'])
                ->label(false);
            ?>
        </div>
        </div>
        <div class="col-md-12 ">
            <div class="logo-style">
            <?=
            $form->field($model, 'type_2')
                ->radioList([ 4 => 'Zərif', 5 => 'Sərt',3=>'Neytral'])
                ->label(false);
            ?>
            </div>
        </div>
<!--        <div class="col-md-12 ">-->
<!--            <div class="logo-style">-->
<!--            --><?//=
//            $form->field($model, 'type_3')
//                ->radioList([ 7 => 'Abstrakt', 8 => 'Hərfi',3=>'Neytral'])
//                ->label(false);
//            ?>
<!--            </div>-->
<!--        </div>-->
        <div class="col-md-12 ">
            <div class="logo-style">
            <?=
            $form->field($model, 'type_4')
                ->radioList([ 10 => 'Əyləncəli', 11 => 'Ciddi',9=>'Neytral'])
                ->label(false);
            ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'save'), ['class' => 'btn btn-color btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
    .logo_type {

    }
    .logo_type label {
        padding: 0 20px;
        min-width: 115px;
    }
    .logo-style {
        align-items: center;
        vertical-align: middle;
        border: 2px solid #eee;
        position: relative;
        display: block;
        display: flex;
        margin-bottom: 10px;
        /* margin-top: 10px; */
        /* justify-content: center; */
    }
    .logo-style .form-group {
        margin-bottom: 0;
        padding-top: 10px;
    }
</style>
