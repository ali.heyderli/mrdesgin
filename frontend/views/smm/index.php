<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LogoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'smm');
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li><a href="<?= Url::to(['app/index']) ?>"><?= Yii::t('main','profile') ?></a></li>
                            <li ><a href="<?= Url::to(['post/index']) ?>"><?= Yii::t('main','order_post') ?></a></li>
                            <li><a href="<?= Url::to(['logo/index']) ?>"><?= Yii::t('main','order_logo') ?></a></li>
                            <li class="active"><a href="<?= Url::to(['/smm']) ?>"><?= Yii::t('main','smm') ?></a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?=Yii::t('main','logout')?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <div style="margin-bottom: 20px">
                            <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                            <strong><?=$this->title?></strong>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12 col-md-12">
                                <div class="card">
                                </div>
                                <div class="card-body">
                                    <table class="table table-responsive-sm table-striped">
                                        <tbody>
                                        <tr>
                                            <td><?= Yii::t('main','active_smm_plan') ?></td>
                                            <td><span class="badge badge-success"><?= Yii::$app->user->identity->smmSubscribe->plan->name?></span></td>
                                        </tr>
                                        <?php $features = explode(',', Yii::$app->user->identity->smmSubscribe->plan->plan) ?>
                                        <tr>
                                            <td>Etibarlıdır :</td>
                                            <td><span class="badge badge-success"><?= date("Y-m-d", strtotime("+1 month", Yii::$app->user->identity->smmSubscribe->created_at))?></span></td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
