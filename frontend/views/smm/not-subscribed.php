<?php

/**
 * Created by PhpStorm.
 * User: ali
 * Date: 2019-01-10
 * Time: 19:49
 */

use yii\helpers\Url; ?>
<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li class=""><a href="<?= Url::to(['app/index']) ?>">Hesabım</a></li>
                            <li class=""><a href="<?= Url::to(['post/index']) ?>">Post sifarişi</a></li>
                            <li class=""><a href="<?= Url::to(['logo/index']) ?>">Logo sifarişi</a></li>
                            <li class="active"><a href="<?= Url::to(['smm/index']) ?>">SMM sifarişi</a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?= Yii::t('main', 'logout') ?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                        <strong><?= $this->title ?></strong>
                        <h3 style="text-align: center;"><?= Yii::t('main', 'You are not subscribed') ?></h3>
                        <div style="    text-align: center;
    margin-top: 40px;">
                            <a class="btn btn-color btn-sm" href="<?=Url::to(['/posts'])?>" data-custom-bg="rgb(255, 213, 0)" style="background-color: rgb(255, 213, 0);" tabindex="0">
                                <?= Yii::t('main', 'View our post pacckages') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

