<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Portfolio'
?>
<section class="page-title parallax-section">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-55.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">
                <div class="title text-center">
                    <h4 class="upper"></h4>
                    <h1>Portfolio</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <ul id="filters">
                <?php foreach ($p_categories as $p_category):?>
                    <li <?= ($p_category->id == 4) ? 'class="active"' : ""?>data-filter=".<?=$p_category->id?>"><?=$p_category->name?></li>
                <?php endforeach;?>
            </ul>

            <div class="with-spacing six-col" id="works-grid">
                <?php foreach($portfolios_logo as $portfolio_logo):?>
                    <div class="work-item <?=$portfolio_logo->pcategory->id?>">
                        <div class="work-detail" style="border: 1px solid #dad6d6f5">
                            <a href="<?=Yii::getAlias('@portfolio').'/'.$portfolio_logo->image_f?>" data-lightbox="<?=$portfolio_logo->image_f?>">
                                <img src="<?=Yii::getAlias('@portfolio').'/'.$portfolio_logo->image_f?>" alt="">
                                <!--                                <div class="work-info">-->
                                <!--                                    <div class="centrize">-->
                                <!--                                        <div class="v-center">-->
                                <!--                                            <h3>Mux Cards</h3>-->
                                <!--                                            <p>Branding, UI/UX</p>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
                <?php foreach($portfolios_post as $portfolio_post):?>
                    <div class="work-item  <?=$portfolio_post->pcategory->id?>">
                        <div class="work-detail">
                            <a href="<?=Yii::getAlias('@portfolio').'/'.$portfolio_post->image_f?>" data-lightbox="<?=$portfolio_post->image_f?>">
                                <img src="<?=Yii::getAlias('@portfolio').'/'.$portfolio_post->image_f?>" alt="">
                                <!--                                <div class="work-info">-->
                                <!--                                    <div class="centrize">-->
                                <!--                                        <div class="v-center">-->
                                <!--                                            <h3>Mux Cards</h3>-->
                                <!--                                            <p>Branding, UI/UX</p>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
                <?php foreach($portfolios_animation as $portfolio_ani):?>
                    <div class="work-item  <?=$portfolio_ani->pcategory->id?>">
                        <div class="work-detail">
                            <a href="<?=Yii::getAlias('@portfolio').'/'.$portfolio_ani->image_f?>" data-lightbox="<?=$portfolio_ani->image_f?>">
                                <img src="<?=Yii::getAlias('@portfolio').'/'.$portfolio_ani->image_f?>" alt="">
                                <!--                                <div class="work-info">-->
                                <!--                                    <div class="centrize">-->
                                <!--                                        <div class="v-center">-->
                                <!--                                            <h3>Mux Cards</h3>-->
                                <!--                                            <p>Branding, UI/UX</p>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>