<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li><a href="<?= Url::to(['app/index']) ?>"><?= Yii::t('main','profile') ?></a></li>
                            <li class="active"><a href="<?= Url::to(['post/index']) ?>"><?= Yii::t('main','order_post') ?></a></li>
                            <li><a href="<?= Url::to(['logo/index']) ?>"><?= Yii::t('main','order_logo') ?></a></li>
                            <li><a href="<?= Url::to(['/smm']) ?>"><?= Yii::t('main','smm') ?></a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?=Yii::t('main','logout')?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <div style="margin-bottom: 20px;">
                            <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                            <strong><?=$this->title?></strong>
                            <?= Html::a(Yii::t('main', 'create_post'), ['create'], ['class' => 'btn btn-color btn-sm']) ?>
<!--                            --><?//= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
<!--                            --><?//= Html::a(Yii::t('main', 'delete'), ['delete', 'id' => $model->id], [
//                                'class' => 'btn btn-danger btn-sm',
//                                'data' => [
//                                    'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
//                                    'method' => 'post',
//                                ],
//                            ]) ?>
                            <a style="float: right;" href="<?= Url::to(Yii::$app->request->referrer)?>"><i class="fa fa-undo"></i> Geri</a>
                        </div>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'title',
                                'description:ntext',
                                [
                                        'attribute' => 'category_id',
                                    'value' => function($data) {
                                        return \common\models\Post::getCategory($data->category_id);
                                    }
                                ],
                                'due_date',
                                [
                                    'attribute' => 'complated',
                                    'value' => function($data) {
                                        return ($data->completed == 0) ? 'Hazır deyil' : 'Hazırlanıb';
                                    }
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'value' => function($data) {
                                        return date('Y-m-d H:i',$data->created_at);
                                    }
                                ],
                            ],
                        ]) ?>
                        <div class="row">
                            <h4 style="text-align: center">Əlavə olunmuş material</h4>
                            <?php foreach ($model->images as $image):?>
                                <div class="col-md-4">
                                    <a href="<?=Yii::getAlias('@post/').$image->image?>">
                                        <img class="img-fluid" src="<?=Yii::getAlias('@post/').$image->image?>">
                                    </a>

                                </div>

                            <?php endforeach;?>
                        </div>
                        <div style="margin-bottom: 20px;">
                            <h4>Hazırlanmış post</h4>
                            <?php if($model->complete): ?>
                                <a class="btn btn-color btn-sm" target="_blank" href="<?= Yii::getAlias('@post').'/'.$model->complete->image ?>">Yüklə</a>
                                <?= Html::a(Yii::t('main', 'Bəyənmədim'), ['beyenmedim', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
//                                    'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                                <div>
                                    <h4>Hazırlanmış post haqqında qeydlər</h4>
                                    <?= $model->complete->caption ?>
                                </div>
                            <?php else:?>
                                <span>Sifarişiniz hazır olduqda buradan əlçatan olacaq</span>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .gj-datepicker{
        width: 100% !important;
    }
    #post-due_date {
        width: 75%;
    }
</style>

