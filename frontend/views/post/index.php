<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerCssFile('');
$this->title = Yii::t('main', 'posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li><a href="<?= Url::to(['app/index']) ?>"><?= Yii::t('main','profile') ?></a></li>
                            <li class="active"><a href="<?= Url::to(['post/index']) ?>"><?= Yii::t('main','order_post') ?></a></li>
                            <li><a href="<?= Url::to(['logo/index']) ?>"><?= Yii::t('main','order_logo') ?></a></li>
                            <li><a href="<?= Url::to(['/smm']) ?>"><?= Yii::t('main','smm') ?></a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?=Yii::t('main','logout')?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <div style="margin-bottom: 20px">
                            <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                            <strong><?=$this->title?></strong>
                            <?= Html::a(Yii::t('main', 'create_post'), ['create'], ['class' => 'btn btn-color btn-sm pull-right']) ?>

                        </div>

                        <div class="row justify-content-center">
                            <div class="col-sm-12 col-md-8">
                                <div class="card">

                                    <div class="card-body">
                                        <?php Pjax::begin(); ?>
                                        <div class="col-md-12">
                                            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                                            <?php
                                            $events = array();
                                            $Event = new \yii2fullcalendar\models\Event();
                                            foreach ($posts as $post) {
                                                $Event = new \yii2fullcalendar\models\Event();
                                                $Event->id = $post->id;
                                                $Event->title = $post->title;
                                                $Event->url = Url::to(['post/view', 'id' => $post->id ]);
                                                $Event->start = date('Y-m-d\TH:i:s\Z',$post->due_date);
                                                $events[] = $Event;
                                            }
                                            ?>
                                            <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                                                'events'=> $events,
                                                'options' => [
                                                    'lang' => 'az'
                                                ],
                                            ));
                                            ?>
                                        </div>

                                        <!--    --><?//=
                                        //    GridView::widget([
                                        //        'dataProvider' => $dataProvider,
                                        //        'filterModel' => $searchModel,
                                        //        'columns' => [
                                        //            ['class' => 'yii\grid\SerialColumn'],
                                        //
                                        //            'id',
                                        //            'user_id',
                                        //            'title',
                                        //            'description:ntext',
                                        //            [
                                        //                'attribute' => 'due_date',
                                        //                'value' => function($data) {
                                        //                    return date('Y-m-d', $data->due_date);
                                        //                },
                                        //
                                        //            ],
                                        //            //'completed',
                                        //            //'created_at',
                                        //            //'updated_at',
                                        //
                                        //            ['class' => 'yii\grid\ActionColumn'],
                                        //        ],
                                        //    ]); ?>
                                        <?php Pjax::end(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="card">
                                    <div class="card-header"><strong></strong>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-responsive-sm table-striped">
                                            <tbody>
                                            <tr>
                                                <td><?= Yii::t('main','active_post_plan') ?> :</td>
                                                <td><span class="badge badge-success"><?= $user->postSubscribe->plan->plan_name?></span></td>
                                            </tr>
                                            <tr>
                                                <td><?= Yii::t('main','active_post_count') ?> :</td>
                                                <td><span class="badge badge-danger"><?= $user->postSubscribe->plan->simple_post + $user->postSubscribe->plan->creative_post  - count($posts)?></span></td>
                                            </tr>
                                            <tr>
                                                <td><?= Yii::t('main','simple_posts') ?> :</td>
                                                <td><span class="badge badge-danger"><?= $user->postSubscribe->plan->simple_post?></span></td>
                                            </tr>
                                            <tr>
                                                <td><?= Yii::t('main','creative_posts') ?> :</td>
                                                <td><span class="badge badge-danger"><?= $user->postSubscribe->plan->creative_post?></span></td>
                                            </tr>
                                            <tr>
                                                <td><?= Yii::t('main','photo_shot') ?> :</td>
                                                <td><span class="badge badge-danger"><?= $user->postSubscribe->plan->photo_shot?></span></td>
                                            </tr>
                                            <tr>
                                                <td><?= Yii::t('main','video_shot') ?> :</td>
                                                <td><span class="badge badge-danger"><?= $user->postSubscribe->plan->video_shot?></span></td>
                                            </tr>
                                            <tr>
                                                <td><?= Yii::t('main','animation') ?> :</td>
                                                <td><span class="badge badge-danger"><?= $user->postSubscribe->plan->animation?></span></td>
                                            </tr>
                                            <tr>
                                                <td><?= Yii::t('main','end_date') ?> :</td>
                                                <td><span class="badge badge-success"><?= date("Y-m-d", strtotime("+1 month", $user->postSubscribe->created_at))?></span></td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function(start, end) {
        var title = prompt('Event Title:');
        var eventData;
        if (title) {
            eventData = {
                title: title,
                start: start,
                end: end
            };
            $('#w0').fullCalendar('renderEvent', eventData, true);
        }
        $('#w0').fullCalendar('unselect');
    }
    function(calEvent, jsEvent, view) {

        alert('Event: ' + calEvent.title);
        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        alert('View: ' + view.name);

        // change the border color just for fun
        $(this).css('border-color', 'red');

    }
</script>
<style>
    .fullcalendar h2 {
        font-size: 17px;
    }
    .fullcalendar .btn {
        line-height: 0px !important;
    }
</style>