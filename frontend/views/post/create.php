<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = Yii::t('main', 'Create Post');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-title parallax-section" style="height:80px;padding-top: 0; padding-bottom: 0">
    <div class="row-parallax-bg">
        <div class="parallax-wrapper">
            <div class="parallax-bg">
                <img src="/layout/images/bg-image-30.jpg" alt="">
            </div>
        </div>
        <div class="parallax-overlay"></div>
    </div>
    <div class="centrize">
        <div class="v-center">
            <div class="container">

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box-app">
                        <ul class="app-navigation">
                            <li><a href="<?= Url::to(['app/index']) ?>"><?= Yii::t('main','profile') ?></a></li>
                            <li class="active"><a href="<?= Url::to(['post/index']) ?>"><?= Yii::t('main','order_post') ?></a></li>
                            <li><a href="<?= Url::to(['logo/index']) ?>"><?= Yii::t('main','order_logo') ?></a></li>
                            <li><a href="<?= Url::to(['/smm']) ?>"><?= Yii::t('main','smm') ?></a></li>
                            <li><a href="<?= Url::to(['site/logout']) ?>"><?=Yii::t('main','logout')?></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="icon-box-app">
                        <i style="font-size: 20px;" class="fa fa-paper-plane"></i>
                        <strong><?=$this->title?></strong>
                        <a style="float: right;" href="<?= Url::to(Yii::$app->request->referrer)?>"><i class="fa fa-undo"></i> <?=Yii::t('main','back')?></a>

                        <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .gj-datepicker{
        width: 100% !important;
    }
    #post-due_date {
        width: 75%;
    }
</style>




