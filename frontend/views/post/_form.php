<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js', ['position' => \yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css');

?>

<div class="post-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'due_date')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'category_id')->dropDownList(\common\models\Post::getPostCategories(),['prompt' => Yii::t('main','select_category')])->label(false) ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px">
    <?= FileInput::widget([
        'name' => 'Post[images][]',
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
    ]);?>
        </div>
    </div>
    <div class="row">
    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('main', 'save'), ['class' => 'btn btn-color btn-sm']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs("
 $('#post-due_date').datepicker({
            uiLibrary: 'bootstrap4'
        });");
?>
