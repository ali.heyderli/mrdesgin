<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CallForm extends Model
{
    public $phone;
    public $name;
    public $email;
    public $subject;
    public $message;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'message'], 'required'],
            [['name', 'email', 'subject', 'message'], 'string']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('main','name'),
            'email' => Yii::t('main','email'),
            'subject' => Yii::t('main','subject'),
            'message' => Yii::t('main','message'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom([Yii::$app->params['adminEmail'] => $this->name])
            ->setSubject('back call')
            ->setTextBody($this->phone)
            ->send();
    }
}
