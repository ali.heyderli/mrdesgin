<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * SendSms form
 */
class SendSmsForm extends Model
{
    public $target_id;
    public $sms_name_id;
    public $quantity;
    public $date;
    public $text;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['target_id','sms_name_id','quantity'],'integer'],
            [['target_id','sms_name_id','quantity'],'required'],

        ];
    }

}
