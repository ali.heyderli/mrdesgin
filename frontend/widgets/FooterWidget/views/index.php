<?php
use yii\helpers\Url;
use common\models\StaticData;

?>
<footer id="footer">
<!--    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <figure class="footer-logo mb-15">
                            <img src="images/logo-dark.png" alt="">
                        </figure>
                        <div class="textwidget">
                            <p>
                                387 Madison Ave
                                <br>New York, NY 10118</p>
                            <p>hello@bezel.co
                                <br>+1 301-736-4321</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h5>The Company</h5>
                        <div class="menu-footer">
                            <ul>
                                <li><a href="#">About Us</a>
                                </li>
                                <li><a href="#">What We Do</a>
                                </li>
                                <li><a href="#">Our Vision</a>
                                </li>
                                <li><a href="#">Portfolio</a>
                                </li>
                                <li><a href="#">Careers</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h5>Recent Posts</h5>
                        <div class="menu-footer">
                            <ul>
                                <li><a href="#">You Can’t Handle Truth</a>
                                </li>
                                <li><a href="#">How To Add Value To Your Life</a>
                                </li>
                                <li><a href="#">A Letter to My 12-Year-Old Self</a>
                                </li>
                                <li><a href="#">Weak Words Kill Experiences</a>
                                </li>
                                <li><a href="#">An Almost Static Stack</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h5>Popular Tags</h5>
                        <div class="tagcloud"><a href="#">Business</a><a href="#">Design</a><a href="#">Development</a><a href="#">Inspiration</a><a href="#">Lifestyle</a><a href="#">Tips</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->    <div class="footer-copy">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="social-list">
                        <li class="social-item-facebook"><a target="_blank" href="https://facebook.com/Mr-Design-428971277306139/"><i class="hc-facebook"></i></a>
                        </li>
                        <li class="social-item-instagram"><a target="_blank" href="https://instagram.com/mrdesign_group"><i class="hc-instagram"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <div class="copy-text">
                        <p>© <?=date('Y').' '.Yii::$app->name?>. <?= Yii::t('main','all_right_reserved') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>