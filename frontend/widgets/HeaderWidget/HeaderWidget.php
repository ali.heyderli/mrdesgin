<?php

namespace frontend\widgets\HeaderWidget;
use backend\models\Page;
use common\models\Language;
use common\models\Menu;
use common\models\Slider;
use common\models\StaticImages;
use frontend\models\Wishlist;
use frontend\modules\product\models\SellType;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use backend\models\Currency;
use common\components\currency\CurrencyRate;

class HeaderWidget extends Widget
{
    private $sliders;
    private $username;
    private $pages;
    private $url;
    private $sellTypes;
    private $isHome = false;
    private $rate = array();
    private $wishlist;

    public function init()
    {
        parent::init();

        $controllerName = Yii::$app->controller->id;
        $actionName = Yii::$app->controller->action->id;

        $this->url = Yii::$app->request->url;
        if ($controllerName == 'site' && $actionName == 'index') {
            $this->isHome = true;
        }
        if ($controllerName == 'site' && $actionName == 'index2') {
            $this->isHome = true;
        }

    }
    public function run()
    {
        return $this->render('header',[
            'isHome' => $this->isHome,
            'url' => $this->url,
            'rates' => $this->rate,

            ]);
    }
}