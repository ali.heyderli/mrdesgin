<?php
use common\models\Menu;
use thyseus\message\models\Message;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use common\components\currency\CurrencyHelper;
use yii\widgets\ActiveForm;


?>
<nav id="navbar">
    <div class="navbar-wrapper">
        <div class="container">
            <div class="logo">
                <a href="<?=Url::to(['/'])?>">
                    <img class="logo-light" src="/home/img/logo4.png" alt="">
                    <img class="logo-dark" src="/home/img/logo6.png" alt="">
<!--                    <img class="logo-light" src="/home/img/mr-design.png" alt="">-->
<!--                    <img class="logo-dark" src="/home/img/mr-design.png" alt="">-->
                </a>
            </div>
            <div class="menu-extras">
                <div class="menu-item">
                    <div class="header-socials">
                        <ul>
                            <li class="dropdown">
                                <a href="#lala" class="dropdown-toggle" data-toggle="dropdown" >
                                    <?=ucfirst(Yii::$app->language)?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="listbox" style="min-width: 60px">
                                    <?php foreach ($_SESSION['languages'] as $lang):?>
                                    <?php if($lang != Yii::$app->language):?>
                                        <li>
                                            <a target="" href="<?=Url::to(['/site/set-lang', 'language' => $lang])?>"><?=ucfirst($lang)?></a>
                                        </li>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </ul>
                            </li>
<!--                            <li class="dropdown">-->
<!--                                <a href="#lala" class="dropdown-toggle" data-toggle="dropdown" >-->
<!--                                    --><?//=ucfirst($_SESSION['currency'])?>
<!--                                    <b class="caret"></b>-->
<!--                                </a>-->
<!--                                <ul class="dropdown-menu" role="listbox" style="min-width: 60px">-->
<!--                                    --><?php //foreach ($_SESSION['currencies'] as $curr):?>
<!--                                        --><?php //if($curr != $_SESSION['currency']):?>
<!--                                            <li>-->
<!--                                                <a target="" href="--><?//=Url::to(['/site/set-currency', 'currency' => $curr])?><!--">--><?//=ucfirst($curr)?><!--</a>-->
<!--                                            </li>-->
<!--                                        --><?php //endif;?>
<!--                                    --><?php //endforeach;?>
<!--                                </ul>-->
<!--                            </li>-->
                            <?php if(Yii::$app->user->isGuest){?>
                                <a class="btn btn-color btn-sm" href="<?=Url::to(['/login'])?>" data-custom-bg="rgb(212, 76, 88)" style="background-color: rgb(212, 76, 88);" tabindex="0"><?=Yii::t('main','login')?></a>
                            <?php } else {?>
                                <a class="btn btn-color btn-sm" href="<?=Url::to(['/app'])?>" data-custom-bg="rgb(212, 76, 88)" style="background-color: rgb(212, 76, 88);" tabindex="0"><?=Yii::t('main','profile')?></a>
                            <?php }?>
                        </ul>
                    </div>
                </div>
                <div class="menu-item">
                    <div class="nav-toggle">
                        <a class="menu-toggle" href="#">
                            <div class="hamburger">
                                <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="navigation">
                <ul class="navigation-menu nav">
                    <li class="menu-item-has-children">
                        <a href="."><?=Yii::t('main','home')?></a>
                    </li>
                    <li class=""><a href="<?=Url::to(['/portfolio'])?>"><?=Yii::t('main','portfolio')?></a>
                    </li>
                    <li class="menu-item-has-children"><a href="#"><?= Yii::t('main','services') ?></a>
                        <ul class="submenu">
                            <li>
                                <a href="<?=Url::to(['/posts'])?>"><?=Yii::t('main','order_post')?></a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['/logo'])?>"><?=Yii::t('main','order_logo')?></a>
                            </li>
<!--                            <li>-->
<!--                                <a href="--><?//=Url::to(['/smm-page'])?><!--">--><?//=Yii::t('main','order_smm')?><!--</a>-->
<!--                            </li>-->
                        </ul>
                    </li>
                    <li class=""><a href="<?=Url::to(['/about'])?>"><?=Yii::t('main','about_us')?></a>
                    </li>
                    <li class=""><a href="<?=Url::to(['/contact'])?>"><?=Yii::t('main','contact')?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<?php if($isHome):?>
    <section class="section-skewed section-bordered">
        <div id="home-slider">
            <div class="slide-item">
                <div class="video-wrapper" data-fallback-bg="/layout/images/bg-image-12.jpg">
                    <div class="video-player">
                        <video autoplay loop preload="auto">
                            <source src="/layout/images/back_video.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
                <div class="slide-wrap dark-overlay">
                    <div class="slide-content">
                        <div class="container">
                            <h1 class="font-small" style="font-size: 37px"><?= Yii::t('main','slider_text'); ?>  <span class="typed-words" data-strings="[&quot;<?= Yii::t('main','slider_text_key1'); ?>&quot;, &quot;smm <?= Yii::t('main','slider_text_key2'); ?>&quot;, &quot;<?= Yii::t('main','slider_text_key3'); ?>&quot;]"></span> <?= Yii::t('main','slider_text_end'); ?></h1>
                            <p><a class="btn btn-light-out" href="<?=Url::to(['site/about'])?>"><?=Yii::t('main','who_we_are')?></a>
                                <a class="btn btn-color" href="<?=Url::to(['/portfolio'])?>" data-custom-bg="rgb(212, 76, 88)"><?=Yii::t('main','portfolio')?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>
