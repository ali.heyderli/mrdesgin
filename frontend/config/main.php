<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'language' => 'az',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'name' => 'Logopob',
    'version' => '1.1',
    'components' => [
        'request' => [
        'csrfParam' => '_csrf-frontend',
        'class' => 'common\components\Request',
        'web'=> '/frontend/web'
    ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                     'jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [

                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [

                    ]
                ]
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'class' => 'common\components\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                '/login' => 'site/login',
                '/about' => 'site/about',
                '/signup' => 'site/signup',
                '/send' => 'site/send-sms',
                '/logout' => 'site/logout',
                '/contact' => 'site/contact',
                '/call-back' => 'site/call-back',
                '/post' => 'post/index',
                '/posts' => 'post/index2',
                '/logo' => 'logo/index2',
                '/logos' => 'logo/index',
                '/smm' => 'smm/app-page',
                '/smm-page' => 'smm/index',
                '/subscribe/<id:\d+>/<type:\d+>' => 'site/subscribe',
                '/app' => 'app/index',
                '/order/callback' => 'site/order-callback',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w\-]+>'=>'<module>/<controller>/<action>',
            ],
        ],

    ],
    'params' => $params,
];
